instructions to install and run razthings.py

if you've run the program before, you can skip this step of installing external packages:
    install necessary packages - run these commands, in this order (copy and paste for best results):
        # serial port access
            sudo apt-get install python-serial
            
        # gpio access
            sudo apt-get install python-rpi.gpio
            
        
put all of the program files in the same directory, replacing (and removing) all old contents:
    razthings.py  (the main program)
    razthings.ini (the configuration file)
    razstuff.py   (utility functions and classes)

    
edit the razthings.ini file (you MUST enter these values!):
    in the [cosm] section:
        modify apikey with your WRITABLE cosm api key value
        optionally, modify the names of the Datastreams (alphanumeric, no spaces)
         (invalid characters will be stripped, spaces will be changed to underscores)
        modify post to enable or disable posting to Cosm (either yes or no)
         values other than the word yes in lower case is evaluated as meaning no            
         
    in the razthings section:
        be sure there's an entry for each razthing that you want logged
        be sure there's a corresponding [razthing-xxxx] section for each razthing
    
    in the [razthing-xxxx] sections:
        the name only appears in the console output
        be sure to provide a unique and valid cosmfeed value
        invalid or unused id values are ok, but if an id matches a real razthing id, 
         then cosmfeed must be a valid (and unique) cosm feed number, 
         or 0 to bypass posting this razthing's data to cosm
        for each of the sense* values, enter the appropriate sen.se feed number
         or 0 to bypass posting this razthing's data to sen.se
        enter an integer rate value to specify the number of seconds between Cosm and Sen.se posts 
         use 0 to disable posting data for this razthing
        for program efficiency, keep unused razthing-xxxx items commented out
        
    in the [ljdemo] section:
        enter the id of the razthing that will control the demo
        enter the load number that is controlled by the x-axis (side-to-side) tilting
        enter the load number that is controlled by the y-axis (front-to-back) tilting
        enter the host ip address of the device running the litejet monitor
        enter the host port number used to access the litejet monitor
        do not delete this section, even if you are not able to use the litejet demo

    in the [display] section:
        set values to yes or no to set the default data that's displayed on the console
            values other than the word yes in lower case is evaluated as meaning no
        

run the program with this command:
    sudo python razthings.py

    
data will be displayed on the console and optionally posted to Cosm!


while the program is running:
    pressing ^C or X<enter> will terminate the program
    pressing C<enter> will toggle logging to cosm
    pressing N<enter> will toggle logging to sen.se
    pressing P<enter> will toggle display of the packet info
    pressing R<enter> will toggle display of the raw data
    pressing T<enter> will toggle display of the temperature data
    pressing G<enter> will toggle display of the gap info
    pressing A<enter> will toggle display of the axis info
    pressing V<enter> will toggle display of the voltage info
    pressing S<enter> will toggle display of the signal strength info
    pressing M<enter> will toggle display of message delta info
    pressing U<enter> will toggle display of the ultrasound info
    pressing L<enter> will toggle display of the light data
    pressing J<enter> will toggle the litejet demo
        don't start the litejet demo if you aren't running the jeffgo litejet monitor
        

    