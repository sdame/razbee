#usr/bin/env python

#razthings.py
# display incoming remote sensor data from a RazThing
#  and post data to Cosm.com    

from __future__ import print_function
import atexit
import datetime
import signal
import sys
import xml.dom.minidom

###########################################################
# LJ DIMMING DEMO
import socket
###########################################################

import razstuff


RAZTHINGS_VERSION = "20130113a"  #<===== KEEP THIS VERSION UPDATED!


def ctrlC_handler(signal, frame): #gracefully handle the ctrl-C
    print(" ctrl-C was pressed.  Exiting. . .")
    sys.exit(0)

def byebye(): #clean up and exit
    now = datetime.datetime.now()
    try:
        rb.close() #close the serial port
    except:
        pass #hadn't opened yet. . .
        
###########################################################
# LJ DIMMING DEMO
    stop_ljdemo()
###########################################################

    print("Exiting.  Ran for " + str(now - starttime))
    print("Done. . .")
    #program ends

###########################################################
# LJ DIMMING DEMO
def tell_ljmon(msg):  #send a message to LJ Monitor
    try:
        sockljmon.send(msg + "\r\n")
    except socket.error, msg:
        print("Error sending LJMon = " + str(msg[0]) + ": " + msg[1])

def start_ljdemo():
    global sockljmon
    print(" Opening socket. . .")
    try:
        sockljmon = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error, msg:
        print("Failed to create socket. Error " + str(msg[0]) + ": " + msg[1])
        sys.exit(1)
    print(" Connecting to socket. . .")
    try:
        sockljmon.connect((ljmonhost,ljmonport))
    except socket.error, msg:
        print("Couldn't connect to port " + str(ljmonport) + ". Error " + str(msg[0]) + ": " + msg[1])
        sys.exit(1)

def stop_ljdemo():
    print(" Closing socket. . .")
    try:
        sockljmon.close()  #close the socket to LJ
    except:
        pass #hadn't opened yet

###########################################################
    
    
### PROGRAM BEGINS HERE ###
starttime = datetime.datetime.now()

signal.signal(signal.SIGINT, ctrlC_handler)  #trap ctrl-c for a cleaner exit
atexit.register(byebye)  #to close up neatly we'll call byebye() when the script is exiting

print("razthings V" + RAZTHINGS_VERSION)
print("Starting " + str(starttime))


#color output
#to use colors: cxxx.format("text to be in cxxx color")
cblk = "\033[22;30m{0}\033[00m" # black
cdgry = "\033[01;30m{0}\033[00m" # dark gray
cgry = "\033[22;37m{0}\033[00m" # gray
cwht = "\033[01;37m{0}\033[00m" # white

cbrn = "\033[22;33m{0}\033[00m" # brown

cred = "\033[22;31m{0}\033[00m" # red
cgrn = "\033[22;32m{0}\033[00m" # green
cblu = "\033[22;34m{0}\033[00m" # blue
clred = "\033[01;31m{0}\033[00m" # light red
clgrn = "\033[01;32m{0}\033[00m" # light green
clblu = "\033[01;34m{0}\033[00m" # light blue

ccyn = "\033[22;36m{0}\033[00m" # cyan
cmag = "\033[22;35m{0}\033[00m" # magenta
cyel = "\033[01;33m{0}\033[00m" # yellow
clcyn = "\033[01;36m{0}\033[00m" # light cyan
clmag = "\033[01;35m{0}\033[00m" # light magenta

#To see the resulting colors and their names, uncomment the next few lines. . .
#color = cdgry.format("cdgry") + cgry.format("cgry") + cwht.format("cwht") + cbrn.format("cbrn") + \
#        cred.format("cred") + cgrn.format("cgrn") + cblu.format("cblu") + clred.format("clred") + \
#        clgrn.format("clgrn") + clblu.format("clblu") + ccyn.format("ccyn") + cmag.format("cmag") + \
#        cyel.format("cyel") + clcyn.format("clcyn") + clmag.format("clmag")
#print(color)
   
#init serial port and RazBee radio
rb = razstuff.RazBeeRadio(38400, True) 

#get parameters from the XML file
dom = xml.dom.minidom.parse("razthings.xml") #must be in the program directory

postToCosm = (dom.getElementsByTagName("post")[0].childNodes[0].data == "yes")
if postToCosm:
    cosmkey = dom.getElementsByTagName("apikey")[0].childNodes[0].data #the cosm api key
    axname = razstuff.clean_ds_name(dom.getElementsByTagName("accelxname")[0].childNodes[0].data) #the cosm feed name for accel x
    ayname = razstuff.clean_ds_name(dom.getElementsByTagName("accelyname")[0].childNodes[0].data) #the cosm feed name for accel y
    azname = razstuff.clean_ds_name(dom.getElementsByTagName("accelzname")[0].childNodes[0].data) #the cosm feed name for accel z
    asname = razstuff.clean_ds_name(dom.getElementsByTagName("accelstatname")[0].childNodes[0].data) #the cosm feed name for accel status
    btname = razstuff.clean_ds_name(dom.getElementsByTagName("batteryname")[0].childNodes[0].data) #the cosm feed name for battery voltage
    mdname = razstuff.clean_ds_name(dom.getElementsByTagName("msgdeltaname")[0].childNodes[0].data) #the cosm feed name for message delta
    rsname = razstuff.clean_ds_name(dom.getElementsByTagName("rssiname")[0].childNodes[0].data) #the cosm feed name for rssi
    tpname = razstuff.clean_ds_name(dom.getElementsByTagName("temperaturename")[0].childNodes[0].data) #the cosm feed name for temperature
    orname = razstuff.clean_ds_name(dom.getElementsByTagName("orientationname")[0].childNodes[0].data) #the cosm feed name for orientation status
else:
    cosmkey = "0"
    print("*** NOT POSTING TO COSM ***")

#get the names and Cosm Feed numbers of the RazThings and make dictionaries to look up values by thingids
rt = {}
rt = razstuff.get_razthings(dom.getElementsByTagName("razthing"), cosmkey, starttime)

###########################################################
# LJ DIMMING DEMO
#get rtid of controlling RazThing
#get xaxis load number
#get yaxis load number

ljrtid = dom.getElementsByTagName("ljdemortid")[0].childNodes[0].data
ljxload = dom.getElementsByTagName("ljdemoloadx")[0].childNodes[0].data
ljyload = dom.getElementsByTagName("ljdemoloady")[0].childNodes[0].data
ljmonhost = dom.getElementsByTagName("ljdemohost")[0].childNodes[0].data #"192.168.1.223"
ljmonport = int(dom.getElementsByTagName("ljdemoport")[0].childNodes[0].data) #6191
###########################################################


#In the main loop:    
# pressing X<enter> will terminate the loop (and the program)
# pressing P<enter> will toggle display of the packet data
# pressing R<enter> will toggle display of the raw data
# pressing T<enter> will toggle display of the temperature data
# pressing G<enter> will toggle display of the gap info
# pressing A<enter> will toggle display of the axis/accel info
# pressing V<enter> will toggle display of the voltage info
# pressing S<enter> will toggle display of the signal strength info
# pressing M<enter> will toggle display of the message delta average
# pressing C<enter> will toggle posting to Cosm
# pressing L<enter> will toggle light dimming demo on attached LiteJet (don't use without an attached LiteJet system)
#  set defaults here
showpacket = dom.getElementsByTagName("showpacket")[0].childNodes[0].data == "yes"
showraw = dom.getElementsByTagName("showraw")[0].childNodes[0].data == "yes"
showtemp = dom.getElementsByTagName("showtemperature")[0].childNodes[0].data == "yes"
showgap = dom.getElementsByTagName("showgap")[0].childNodes[0].data == "yes"
showaxis = dom.getElementsByTagName("showaxis")[0].childNodes[0].data == "yes"
showvolt = dom.getElementsByTagName("showvoltage")[0].childNodes[0].data == "yes"
showsig = dom.getElementsByTagName("showsignal")[0].childNodes[0].data == "yes"
showmd = dom.getElementsByTagName("showmsgdelta")[0].childNodes[0].data == "yes"

###########################################################
# LJ DIMMING DEMO
ljdemo = False 
###########################################################

print("And away we go!")
print("Press X<enter> to exit (or P, R, T, G, A, V, S, M). . .")

#loop reading from serial port and from keyboard
looping = True
while looping:
    recmsg = rb.get_sensor_data(rb.length()) #see what's been sent (if anything)
    if len(recmsg) == rb.length(): #if we got the right amount of data
        currmsgt = datetime.datetime.now() #mark the receive time
        
        #parse the incoming data
        rtsequence, rtid, rtxaxis, rtyaxis, rtzaxis, rtaccstat, rttemperature, rtbatvolt, rtrssi = rb.parse(recmsg)

        rt[rtid].currmsgtime = currmsgt
        #TODO: how to display the accel stati every cosmtime seconds when posting to Cosm
        # maybe post these whenever they happen, even if it's not the time to post to Cosm
        
        #accelerometer stati 
        rtaccorient = int(rtaccstat / 256) #separate our orientation bits from the rt status bits
        rtaccstat -= (rtaccorient * 256)
        rtaccbit = razstuff.get_bits(rtaccstat) 
        rtorientbit = razstuff.get_bits(rtaccorient)
        
        #check for a gap in the sequence number
        gap = rt[rtid].seq.check(rtsequence)
        if gap > 0:
            gapmsg = clred.format("JUST MISSED SOME MESSAGES")
            rt[rtid].lastgaptime = str(rt[rtid].currmsgtime)
        else:
            gapmsg = ""
            
        #average the recent values
        rt[rtid].av.add(rtbatvolt) #average voltage
        rt[rtid].ar.add(rtrssi)  #average signal strength

        rt[rtid].at.add(rttemperature) #average temperature
       
        msgdelta = rt[rtid].currmsgtime - rt[rtid].prevmsgtime #time between messages
        mdmicro = msgdelta.microseconds #convert it to microseconds
        rt[rtid].md.add(mdmicro) #averaging time between messages
        
        rt[rtid].prevmsgtime = rt[rtid].currmsgtime
        rt[rtid].msgcount += 1 #count it
        
        print("") #print a blank line between each packet (TODO: ?don't print this if nothing else is being printed?)
        if showpacket:
            print(cwht.format(rtid) + " " + cbrn.format(rt[rtid].rtname) + ": " + cwht.format(str(rtsequence)) + "/" + cwht.format(str(rt[rtid].msgcount)) + ": " + str(rt[rtid].currmsgtime) + " (" + str(msgdelta) + ")")

###########################################################
# LJ DIMMING DEMO
# send the command to the litejet monitor and ignore replies
        if ljdemo:
            #print("rtid=" + rtid + ", ljrtid=" + ljrtid)
            if rtid == ljrtid:
                xljdim = int(round(float(rtxaxis * 1.5),1)) 
                yljdim = int(round(float(rtyaxis * 1.5),1))

                tellit = "@LJDIM:" + str(ljxload).zfill(3) + "," + str(xljdim).zfill(2)
                tell_ljmon(tellit)
                #print("Telling LJMON [" + tellit + "]")
            
                tellit = "@LJDIM:" + str(ljyload).zfill(3) + "," + str(yljdim).zfill(2)
                tell_ljmon(tellit)
                #print("Telling LJMON [" + tellit + "]")
###########################################################

        if showraw:
            rawstr = recmsg.encode("hex")
            rawstr = cwht.format(rawstr[0:2]) + cgry.format(rawstr[2:4]) + cwht.format(rawstr[4:6]) + \
                        clgrn.format(rawstr[6:8]) + clred.format(rawstr[8:12]) + clcyn.format(rawstr[12:14]) + \
                        clmag.format(rawstr[14:16]) + cyel.format(rawstr[16:18]) + clgrn.format(rawstr[18:20]) + \
                        cwht.format(rawstr[20:22]) + cred.format(rawstr[22:26]) + cgrn.format(rawstr[26:30]) + \
                        clcyn.format(rawstr[30:32]) + cwht.format(rawstr[32:])              
            print(" [" + rawstr + "]") #and display it
            
        if showtemp:
            avgtemp = rt[rtid].at.average(1) #get the average temperature so we can display it on the console
            print(" Temperature recent average=" + cgrn.format(str(avgtemp)) + ", immediate=" + cyel.format(str(rttemperature)))
            if rt[rtid].at.maximum > 0:
                print("  Max=" + cred.format(str(rt[rtid].at.maximum)) + " at " + str(rt[rtid].at.maxtime) + "  Min=" + ccyn.format(str(rt[rtid].at.minimum)) + " at " + str(rt[rtid].at.mintime) +  ")")
                
        if showgap:
            print(" " + str(rt[rtid].seq.gaps) + " gaps, last gap " + rt[rtid].lastgaptime + " " + gapmsg)
            
        if showaxis:
            print(" X:" + str(rtxaxis) + ", Y:" + str(rtyaxis) + ", Z:" + str(rtzaxis))
            print("  Status bits: " + " ".join(rtaccbit))
            print("  Orientation: " + " ".join(rtorientbit))
            
        if showvolt:
            print(" Battery: " + cred.format(str(rtbatvolt)))
            
        if showsig:
            print(" RSSI: " + cyel.format(str(rtrssi)))
            
        if showmd:
            print(" Average message delta: " + cmag.format(str(round(float(rt[rtid].md.average(3)/1000000.0),3))))
            
        #note: post to Cosm after displaying values on console because after posting we clear the averages. . .
        if postToCosm: #if we're posting to Cosm
            if currmsgt > rt[rtid].nextcosmpost: # and it's time to post
                print(cgrn.format("\nSending data to COSM for ") + clgrn.format(rtid + " " + rt[rtid].rtname) + " (" + str(currmsgt - rt[rtid].prevcosmpost) + ")") 
                rtdata = [(axname, str(rtxaxis)), (ayname, str(rtyaxis)), (azname, str(rtzaxis)), \
                            (asname, str(rtaccstat)), (tpname, str(rt[rtid].at.average(1))), (btname, str(rt[rtid].av.average(2))), \
                            (rsname, str(rt[rtid].ar.average(0))), (orname, str(rtaccorient)), \
                            (mdname, str(round(float(rt[rtid].md.average(3)/1000000.0),3)))]
                rt[rtid].iot.postlist(rtdata) #send the data to Cosm
                rt[rtid].nextcosmpost = currmsgt + datetime.timedelta(seconds=rt[rtid].rate) #set up the next post time
                rt[rtid].prevcosmpost = currmsgt
                rt[rtid].av.clear() #clear the average values from the last set of data
                rt[rtid].ar.clear()
                rt[rtid].at.clear()
                rt[rtid].md.clear()
            
    else:
        if len(recmsg) > 0:
            print(cbrn.format("Skipping short data payload [" + str(len(recmsg)) + "]"))
            
    kbd = razstuff.kbd_ready() #see if anything from the keyboard
    if len(kbd) > 0: 
        choice = kbd[:1] #look at only the first character
        choice = choice.upper()
        
        if choice == "X": #X exits the program
            looping = False #exit the loop, thereby exiting the program (after cleanup)
        elif choice == "P": #P toggles showing packet data
            showpacket = not showpacket
        elif choice == "R": #R toggles showing raw data
            showraw = not showraw
        elif choice == "T": #T toggles showing temperature
            showtemp = not showtemp
        elif choice == "G": #G toggles showing gaps
            showgap = not showgap
        elif choice == "A": #A toggles showing accel/axis data
            showaxis = not showaxis
        elif choice == "V": #V toggles showing voltage
            showvolt = not showvolt
        elif choice == "S": #S toggles showing signal strength
            showsig = not showsig
        elif choice == "M": #M toggles showing message delta average
            showmd = not showmd
        elif choice == "C": #C toggles posting to Cosm
            postToCosm = not postToCosm
            if postToCosm:
                print(clgrn.format("Resuming posting to Cosm"))
            else:
                print(clred.format("Suspending posting to Cosm"))
###########################################################
# LJ DIMMING DEMO
        elif choice == "L": #L toggles the LiteJet dimming demo 
            ljdemo = not ljdemo
            if ljdemo:
                print(clgrn.format("*************** L J D E M O    A C T I V E ***************"))
                start_ljdemo()
            else:
                print(clred.format("*************** L J D E M O    S T O P P E D ***************"))
                stop_ljdemo()
###########################################################

byebye()            
