#razstuff

import datetime
import eeml
import RPi.GPIO as GPIO
import re
import select
import serial
import string
import sys
import thread
import time


class RazThing: 
    def __init__(self, id, name, feed, cosmkey, rate, starttime): 
        self.id = id
        self.rtname = name
        self.rtfeed = feed
        self.rate = int(rate)
        self.iot = Cosm(cosmkey, self.rtfeed)
        self.msgcount = 0
        self.nextcosmpost = starttime + datetime.timedelta(seconds=self.rate)
        self.prevcosmpost = starttime

        self.at = AvgList() #create a list to track and average temperature readings
        self.av = AvgList() #create a list to track and average battery voltages
        self.ar = AvgList() #create a list to track and average RSSI values
        self.md = AvgList() #create a list to track and average message deltas
        self.seq = SeqCheck(255) #to check for consecutive sequence numbers    
        self.lastgaptime = "never"
        self.prevmsgtime = starttime #we'll use these to calculate time between messages
        self.currmsgtime = starttime
        
class RazBeeRadio:
    # open and initialize the serial port
    # the speed is specified by the caller
    def __init__(self, speed, reset):
        if reset:
            self.resetRazbeeRadio() #reset the MC1322x radio chip on the RazBee

        self.ser = serial.Serial("/dev/ttyAMA0")
        self.ser.baudrate = speed
        self.ser.bytesize = 8
        self.ser.parity = "N"
        self.ser.stopbits = 1
        self.ser.timeout = 1
        self.ser.xonxoff = 0
        self.ser.rtscts = 0
        print("Opened serial port " + self.ser.portstr + " @ " + str(self.ser.baudrate))
        self.send_raw("\xfd\x07")
        
    # close closes the serial port (bonus points if you figured that out yourself)
    def close(self):
        self.ser.close()
        print("Closed serial port")


    # send_raw sends a message to the serial port
    def send_raw(self, msg):
        msg = msg 
        self.ser.write(msg)
        print("Sent raw data [" + msg.encode("hex") + "]") #we're done
        
        
    # recv reads at most a requested number of bytes from the serial port
    def recv(self, length):
        while 1: 
            response = self.ser.read(length)
            if len(response) > 0:
                break;
        return response
        

    #try to get the requested bytes of sensor data
    def get_sensor_data(self, sensorlen):
        msg = ""
        i,o,e = select.select([self.ser],[],[],0.0001)
        for s in i:
            if s == self.ser:
                msg = self.recv(sensorlen)
                        
        return msg

    def resetRazbeeRadio(self): #Reset the MC1322x radio chip on the RazBee 
        pin = 12 #GPIO-1 is RPi pin 12 (BCM pin 18)
        value = 0
        states = [GPIO.HIGH, GPIO.HIGH, GPIO.LOW, GPIO.HIGH, GPIO.HIGH]
        print("Resetting MC1322x Radio Chip on RazBee")
        #Set the mode numbering of the pins
        GPIO.setmode(GPIO.BOARD)  #if using GPIO.BCM, use pin=18 for GPIO-1
        GPIO.setwarnings(False)

        GPIO.setup(pin, GPIO.OUT)
        value = GPIO.input(pin)
        print("Beginning state of GPIO-x (pin = " + str(pin) + ") is " + str(value))
        
        for state in states:
            GPIO.output(pin, state)
            value = GPIO.input(pin)
            print("Current state of GPIO-x (pin = " + str(pin) + ") is " + str(value))
            time.sleep(1)


    def length(self):
        return 17 #this is the length of the sensor data payload from the RazBee
        
        
    def parse(self, msg):
        #converts the data from binary to ascii-hex, then converts the values back to binary
        #this isn't the most efficient for dealing with the data for display by this program, but 
        # since it will end up sending the data somewhere else (probably XML), then this is probably ok
        #some values, like temperature and battery voltage, need some extra attention to decode them properly
        msgasc = msg.encode("hex") #convert the data to ascii-hex

        #get the individual data fields from the ascii-hex string
        xrbsequence = msgasc[6:8]
        xrbthingid = msgasc[8:12]
        xrbxaxis = msgasc[12:14]
        xrbyaxis = msgasc[14:16]
        xrbzaxis = msgasc[16:18]
        xrbaccstat = msgasc[18:20]
        xrbtempmsb = msgasc[22:24]
        xrbtemplsb = msgasc[24:26]
        xrbbatmsb = msgasc[26:28]
        xrbbatlsb = msgasc[28:30]
        xrblqi = msgasc[30:32]
        
        #for internal use by this program, convert to binary values
        sequence = self.hex2dec(xrbsequence)

        xaxis = self.hex2dec(xrbxaxis)
        if xaxis > 127:
            xaxis = -(xaxis - 128)
            xneg = 1
        else:
            xneg = 0
        if xaxis < 0:
            xaxis += 128

        yaxis = self.hex2dec(xrbyaxis)
        if yaxis > 127:
            yaxis = -(yaxis - 128)
            yneg = 1
        else:
            yneg = 0
        if yaxis < 0:
            yaxis += 128

        zaxis = self.hex2dec(xrbzaxis)
        if zaxis > 127:
            zaxis = -(zaxis - 128)
            zneg = 1
        else:
            zneg = 0
        if zaxis < 0:
            zaxis += 128
            
        accorient = (zneg * 4) + (yneg * 2) + xneg #bitmap of orientation
        accstat = self.hex2dec(xrbaccstat) 
#        print("orient: " + str(accorient) + ", stat: " + str(accstat))

        accstat += (accorient * 256) 
#        print("combined: "+ str(accstat))
        temperature = self.ctof(self.makefloat(xrbtempmsb, xrbtemplsb))
        batvolt = self.makefloat(xrbbatmsb, xrbbatlsb)
        lqi = self.hex2dec(xrblqi)
        rssi = self.lqi2rssi(lqi)
        
        return sequence, xrbthingid, xaxis, yaxis, zaxis, accstat, temperature, batvolt, rssi
        #return sequence, xrbthingid, xg, yg, zg, accstat, temperature, batvolt, rssi


    def lqi2rssi(self, lqi): #convert Freescale LQI to RSSI (formula per Steve D)
        rssi = (-15) - ((85.0/255.0) * (255-lqi))
        return rssi
        
    def makefloat(self, ahmsb, ahlsb): #convert two bytes to a float: msb is signed integer part, lsb is decimal part
        binmsb = self.hex2dec(ahmsb)
        binlsb = self.hex2dec(ahlsb)
        if binmsb > 127:
            binmsb -= 128
            sign = -1
        else:
            sign = 1
        binlsb /= 100.0
        floatval = (binmsb + binlsb) * sign
        #floatval = float(sign + str(binmsb) + "." + str(binlsb))
        return floatval
        
    def ctof(self, celsius): #convert celcius to fahrenheit
        return float((celsius * 1.8) + 32) #convert to fahrenheit
        
    def hex2dec(self, s):  #return the integer value of a hexadecimal string (ie, "0D" returns 13)
        return int(s, 16)


class Cosm: #init and post to a cosm.com feed
    def __init__(self, key, feed): #caller provides cosm api-key and feed number
        self.api_key = key
        self.feed = feed
        self.api_url = "/v2/feeds/" + str(self.feed) + ".xml"
        self.cosm = eeml.Pachube(self.api_url, self.api_key)
    
    def post(self, stream, data): #post to the feed
        print("Sending [" + str(data) + "] to Datastream " + str(stream) + " at COSM : " + self.api_url)
        self.cosm.update([eeml.Data(stream, data)])
        try:
            self.cosm.put()        
        except Exception as e:
            print("Error sending to Cosm: " + str(e))

    def postlist(self, datalist):
        #print("Calling postlist in a thread")
        try:
           thread.start_new_thread(self.postlist_thread, (datalist,))
        except Exception as e:
           print("Error: unable to start thread: " + str(e))
            
            
    def postlist_thread(self, datalist):
        #print(" In a thread now. . .")
        print("POSTING DATA AT COSM. . .")
        for stream, data in datalist:
            #print("  Sending [" + str(data) + "] to Datastream " + str(stream) + " at COSM : " + self.api_url)
            self.cosm.update([eeml.Data(stream, data)])
        try:
            self.cosm.put()        
        except Exception as e:
            print("ERROR SENDING TO COSM: " + str(e))
        

        
class AvgList:  #creates a rolling list, tracking the last X values and calculating the average of those values
    

    def __init__(self): #create a list for averaging values
        self.initval = 999999
        self.init()
        
    def add(self, newval): #add an item to front of the list, removing the last item from the list
        self.rlist.append(newval)

        if newval > self.maximum: #check for new max value
            self.maximum = newval
            self.maxtime = datetime.datetime.now()
        if newval < self.minimum: #check for new min value
            self.minimum = newval
            self.mintime = datetime.datetime.now()

    def average(self, decimals):
        if len(self.rlist) > 0: #avoid a divide by zero error!
            self.avg = round(float(sum(self.rlist)) / len(self.rlist), decimals)
        else:
            self.avg = 0.0
        return self.avg

    def clear(self):
        self.rlist = list()
        self.start = datetime.datetime.now()
        self.avg = 0

    def init(self):
        self.clear()
        self.maximum = -self.initval
        self.maxtime = self.start
        self.minimum = self.initval
        self.mintime = self.start

        
class SeqCheck: #verifies that consecutive calls to check() are contiguous values without gaps
    def __init__(self, max):
        self.seqmax = max
        self.shouldbe = self.seqmax + 1
        self.init = False
        self.gaps = 0

    def check(self, got): #
        if self.init == True: #if we've received an initial value
            retval = got - self.shouldbe #should be zero if there's no gap in the sequence
            if retval != 0:
                self.gaps += 1 #count the number times we've had gaps in sequences
        else:
            self.init = True #we just got our initial value
            retval = 0 #so the return value is 0 ("no gap")
        self.shouldbe = got + 1 #the next number "should be" the current value plus one
        if self.shouldbe > self.seqmax: #if the next number exceeds the rollover value
            self.shouldbe = 0 #reset the sequence number
        return retval
        
    def gaps(self): #returns the number of times we've had gaps in the sequence
        return self.gaps

        
def kbd_ready(): # returns true if there's data from the keyboard (ending with <enter>)
        i,o,e = select.select([sys.stdin],[],[],0.0001)
        for s in i:
            if s == sys.stdin:
                input = sys.stdin.readline()
                return input #return what was entered
        return "" #return empty string if no keyboard input

    
def clean_ds_name(name, tc=False): #make sure Cosm Datastream names are valid (A-Z a-z 0-9 _)
    if tc:
        name = name.title() #Title Case Each Word If Requested
    name = name.replace(" ","_") #replace_spaces_with_underscores
    name = re.sub(r'\W+','',name) #remove all non alphanumeric characters
    return name

def get_bits(byte): #return 8 values representing the state of each bit in a byte
    #bits = []
    #bits.append((byte & 1) > 0)
    #bits.append((byte & 2) > 0)
    #bits.append((byte & 4) > 0)
    #bits.append((byte & 8) > 0)
    #bits.append((byte & 16) > 0)
    #bits.append((byte & 32) > 0)
    #bits.append((byte & 64) > 0)
    #bits.append((byte & 128) > 0)
    #return bits
    return list(bin(byte)[2:].rjust(8,'0'))

    
def get_xml_txt(nodelist): #get text from a node
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)

def get_razthings(razthings, cosmkey, starttime): #get razthing info from XML file and return a dictonary of RazThing objects
    rtlist = {}
    for r in razthings:
        item = r.getElementsByTagName("id")[0]
        id = get_xml_txt(item.childNodes)
        item = r.getElementsByTagName("name")[0]
        name = get_xml_txt(item.childNodes)
        item = r.getElementsByTagName("feed")[0]
        feed = get_xml_txt(item.childNodes)
        item = r.getElementsByTagName("rate")[0]
        rate = get_xml_txt(item.childNodes)
        
        rtlist[id] = RazThing(id, name, feed, cosmkey, rate, starttime)
    return rtlist
