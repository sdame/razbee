#!/home/pi/.rvm/rubies/ruby-1.9.3-p194/bin/ruby

require "serialport"

#params for serial port
port_str = "/dev/ttyAMA0"
baud_rate = 115200 
data_bits = 8
stop_bits = 1
parity = SerialPort::NONE
 
sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)
 
#just read forever
printf("Loopback test at %d baud\n",baud_rate);
while true do
  sp.putc('1');
  printf("[%c]", sp.getc)
end
 
sp.close
