#!/home/pi/.rvm/rubies/ruby-1.9.3-p194/bin/ruby
require 'wiringpi'
require "serialport"

#===================================ZtcMsg=====================================
# ----------------------------------------------------------------------------
#       Class: ZtcMsg
# Description: Sender class for Zigbee ZTC Messages
# ----------------------------------------------------------------------------
class ZtcMsg
  # --------------------------------------------------------------------------
  # constructor
  # --------------------------------------------------------------------------
  def initialize
    #params for serial port running at 38.4Kbaud
    @port_str = "/dev/ttyAMA0"
    @baud_rate = 38400
    @data_bits = 8
    @stop_bits = 1
    @parity = SerialPort::NONE
    @sp = SerialPort.new(@port_str, @baud_rate, @data_bits, @stop_bits, @parity)
  end

  # --------------------------------------------------------------------------
  # Close the serial port
  # --------------------------------------------------------------------------
  def close
    @sp.close
  end

  # --------------------------------------------------------------------------
  def baud
    return @baud_rate
  end
  # --------------------------------------------------------------------------
  
  # --------------------------------------------------------------------------
  def rxbyte
    return @sp.getbyte
  end 
  # --------------------------------------------------------------------------
  
  # --------------------------------------------------------------------------
  # Send a fully formed ZTC byte string command of the form
  #   "02 A3 08 00 AB"  or "02A30800AB"
  # --------------------------------------------------------------------------
  def ztc_send(ztc_full_msg)
    # strip space separators (if any)
    ztc = ztc_full_msg.gsub(' ','')
    puts ztc
    ztc.scan(/../).map do |x| 
       @sp.putc(x.hex) 
       puts x
    end
  end

end
#===================================ZtcMsg=====================================

lut = {
'06' => 0.0625,
'0D' => 0.125,
'13' => 0.1875,
'19' => 0.25,
'1F' => 0.3125,
'26' => 0.375,
'2C' => 0.4375,
'32' => 0.5,
'38' => 0.5625,
'3F' => 0.625,
'45' => 0.6875,
'4B' => 0.75,
'51' => 0.8125,
'58' => 0.875,
'5E' => 0.9375,
}

# create a new
z = ZtcMsg.new

tm = Time.new.to_s.split(" -")
msg = tm[0].to_s + ":" 
#just read forever
printf("Read UART Port at %d baud\n",z.baud);
while true do
  c = z.rxbyte
  s = sprintf("%02x", c).upcase
  # ----------------- parse protocol bytes ---------------------
  msg << s + " "
  if(s == "FF")
    begin
      f = 0.0
      a = msg.split(':')
      x = a[3].split(' ')
      t = x[11].to_i(16)
      temp = t.to_f 
      f = lut[x[12]].to_f
      temp = temp + f
      
      # check for correct length to trap corrupt packets
      if(x.length == 17)
         puts msg + "T=" + temp.to_s 
      end

      tm = Time.new.to_s.split(" -")
      msg = tm[0].to_s + ":" 
    rescue Exception => e
      puts e.message 
    end
  end
end

z.close

