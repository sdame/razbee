require "serialport"
require "byebug"

#params for serial port
port_str = "/dev/ttyUSB0"
baud_rate = 19200 
data_bits = 8
stop_bits = 1
parity = SerialPort::NONE

d1 = ['04', '9B', 'C2'] ## lamp module that works (links)
d2 = ['04', '80', 'D7'] ## lamp module that doesn't seem to link

d = d1
sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)

   print "Sending Test OFF Message\n"
   message = [] 
   message << "0x02".to_i(16) # INSTEON_PLM_START
   message << "0x62".to_i(16) # INSTEON_STANDARD_MESSAGE
   # device id
   message << d[0].to_i(16) # Addr 1
   message << d[1].to_i(16) # Addr 2
   message << d[2].to_i(16) # Addr 3

   message << "0x0F".to_i(16) # INSTEON_MESSAGE_FLAG
   message << "0x12".to_i(16) # 0x12 = FAST ON, 0x14 = FAST OFF, 0x19 = STATUS
   message << "0x00".to_i(16) # 0x00 = 0%, 0xFF = 100%

   ## =================== SEND OFF =====================
   printf("Read Test at %d baud\n",baud_rate);
   puts 'Sending FAST ON Command to responder:'
   message.each { |c| printf("[%.2X] ",c) }
   message.each { |x| sp.putc(x) }
   sleep(1)
   puts; puts 'Response from PLC'
   i = 0
   while (i < 20) do
      cc = sp.getc
      if cc.nil?
         puts "nil"
      else
         x = cc.each_byte.first
         printf("[%.2X] ",x)
      end 
      i = i+1
   end

   ## =================== SEND OFF =====================
   sleep(1)
   puts
   message[7] = "0xFF".to_i(16)
   puts; puts 'Sending FAST OFF Command to PLC:'
   message.each { |c| printf("[%.2X] ",c) }
   message.each { |x| sp.putc(x) }

   sleep(2)
   puts; puts 'Wait 2 sec, then response from PLC and responder'
   i = 0
   while (i < 20) do
      cc = sp.getc
      if cc.nil? 
         puts "nil"
      else
         x = cc.each_byte.first
         printf("[%.2X] ",x)
      end 
      i = i+1
   end
   puts
   sp.close
   puts 'Serial Port Closed'
