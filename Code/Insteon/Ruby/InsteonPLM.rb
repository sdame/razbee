# InsteonPLM.rb
# <registerProcess name="Insteon PLM" isGateway="true" version="0.6" workingDirectory="/think/ThinkDrivers" commandLine="ruby InsteonPLM.rb" autoStart="false"/>
require 'sinatra'
#require 'rubygems'
require 'serialport'
require 'thread'
require 'rest-client'
require 'json'
require 'em-eventsource'
#require 'byebug'

set :environment, :production
set :port, 8001
enable :run

TA_API_BASE = 'https://thinkautomatic.io/api/'

def localIp
  orig = Socket.do_not_reverse_lookup  
  Socket.do_not_reverse_lookup =true # turn off reverse DNS resolution temporarily
  UDPSocket.open do |s|
    s.connect '64.233.187.99', 1 #google
    s.addr.last
  end
ensure
  Socket.do_not_reverse_lookup = orig
end


class Insteon
	## =======================Insteon Command Table Entry=======================
	## Description: Private class provides a standard structure for each Insteon
	##              command type.  Each entry contains:
	##              string: name of the command
	##                byte: Insteon command code
	##                byte: TODO explain?
	##                byte: TODO explain?
	##                byte:  explain? 
	## =========================================================================
	class InsteonCommandTableEntry
		attr_accessor :strCmdType, :bIMCmd, :bCmd1, :fMatchCmd1Lookup, :fAcked
		## Instance vars for class Insteon Protocol CommandType
	   @strCmdType       # Command name string
	   @bIMCmd				# Message Command byte code
	   @bCmd1				# Command 1
	   @fMatchCmd1Lookup	#
	   @fAcked				#

	   def initialize(strCmdType, bIMCmd, bCmd1, fMatchCmd1Lookup, fAcked)
	   	@strCmdType = strCmdType
	   	@bIMCmd = bIMCmd
	   	@bCmd1 = bCmd1
	   	@fMatchCmd1Lookup = fMatchCmd1Lookup
	   	@fAcked = fAcked
	   end

	   def ==(ict)
	   	return ict.nil? ? false : (@bIMCmd == ict.bIMCmd) && (@bCmd1 == ict.bCmd1)
	   end
	end

	## =========================InsteonCommandTableEntryList=========================
	## Description: Private class providing a lookup table for each 
	##              InsteonCommandTableEntry 
	## => TODO maybe put this config data in a yml file?
	## ==============================================================================
	class InsteonCommandTableEntryList < Array
		attr_accessor :ictError
		@ictError

		def initialize()
			@ictError = InsteonCommandTableEntry.new("Error", 0, 0, false, false)
			push(InsteonCommandTableEntry.new("PressHoldStart", 0x50, 0x17, true, false))
			push(InsteonCommandTableEntry.new("PressHoldStop", 0x50, 0x18, true, false))

			push(InsteonCommandTableEntry.new("GetInstEngVer", 0x62, 0x0E, true, true))
			push(InsteonCommandTableEntry.new("Ping", 0x62, 0x0F, true, true))

#			push(InsteonCommandTableEntry.new("On", 0x62, 0x11, true, true))
			push(InsteonCommandTableEntry.new("On", 0x62, 0x12, true, true))   	# treating On the same as FastOn
			push(InsteonCommandTableEntry.new("Set", 0x62, 0x11, false, true))
#			push(InsteonCommandTableEntry.new("Off", 0x62, 0x13, true, true))
			push(InsteonCommandTableEntry.new("Off", 0x62, 0x14, true, true))		# treating Off the same as FastOn
			push(InsteonCommandTableEntry.new("Dim", 0x62, 0x16, true, true))
			push(InsteonCommandTableEntry.new("Bright", 0x62, 0x15, true, true))
			push(InsteonCommandTableEntry.new("FastOn", 0x62, 0x12, true, true))
			push(InsteonCommandTableEntry.new("FastOff", 0x62, 0x14, true, true))
			push(InsteonCommandTableEntry.new("SprinklerOn", 0x62, 0x40, true, true))
			push(InsteonCommandTableEntry.new("SprinklerOff", 0x62, 0x41, true, true))
			push(InsteonCommandTableEntry.new("EZIO_On", 0x62, 0x45, true, true))
			push(InsteonCommandTableEntry.new("EZIO_Off", 0x62, 0x46, true, true))
			push(InsteonCommandTableEntry.new("Status", 0x62, 0x19, true, true))
			push(InsteonCommandTableEntry.new("X10HouseUnit", 0x63, 0, false, false))
			push(InsteonCommandTableEntry.new("X10HouseCmd", 0x63, 0, false, true))
			push(InsteonCommandTableEntry.new("ResetPLM", 0x67, 0, false, false))
			push(InsteonCommandTableEntry.new("GetPLCInfo", 0x60, 0, false, true))
			push(InsteonCommandTableEntry.new("ReadWrite-ALDB", 0x62, 0x2F, true, true))
			push(InsteonCommandTableEntry.new("GetFirstALL-LinkRecord", 0x69, 0, false, true))
			push(InsteonCommandTableEntry.new("GetNextALL-LinkRecord", 0x6A, 0, false, true))
			push(InsteonCommandTableEntry.new("ALL-LinkController", 0x64, 0x01, false, true))
			push(InsteonCommandTableEntry.new("ALL-LinkResponder", 0x64, 0x00, false, true))
			push(InsteonCommandTableEntry.new("ALL-LinkStart", 0x64, 0x03, false, true))
			push(InsteonCommandTableEntry.new("EZIO-OutputOff", 0x62, 0x46, true, true))
			push(InsteonCommandTableEntry.new("EZIO-AlarmDataRequest", 0x62, 0x47, true, true))
			push(InsteonCommandTableEntry.new("EZIO-WriteOutputPort", 0x62, 0x48, true, true))
			push(InsteonCommandTableEntry.new("EZIO-ReadInputPort", 0x62, 0x49, true, true))
			push(InsteonCommandTableEntry.new("EZIO-GetSensorValue", 0x62, 0x4A, true, true))
			push(InsteonCommandTableEntry.new("EZIO-AlarmDataRequest", 0x62, 0x47, true, true))
			push(InsteonCommandTableEntry.new("EZIO-SetSensorAlarmOff->On", 0x62, 0x4B, true, true)) #alarm activated from Off to On event
			push(InsteonCommandTableEntry.new("EZIO-SetSensorAlarmOn->Off", 0x62, 0x4C, true, true)) #alarm activated with On to Off event
			push(InsteonCommandTableEntry.new("EZIO-WriteConfigPort", 0x62, 0x4D, true, true))
			push(InsteonCommandTableEntry.new("EZIO-ReadConfigPort", 0x62, 0x4E, true, true))
			push(InsteonCommandTableEntry.new("EZIO-Controls", 0x62, 0x4F, true, true)) #Various sub-commands available
		end

		## TODO
		def FindCmdType(strCmdType)
			ret = select {|ict| ict.strCmdType.upcase == strCmdType.upcase}
			return ret.any? ? ret[0] : @ictError
		end

		## TODO
		def FindCmdTypeFromCmd1(bCmd1)
			ret = select {|ict| ict.fMatchCmd1Lookup && ict.bCmd1 == bCmd1}
			return ret.any? ? ret[0] : @ictError
		end

		## TODO
		def FindCmdTypeFromIMCmd(bIMCmd)
			ret = select {|ict| ict.bIMCmd == bIMCmd}
			return ret.any? ? ret[0] : @ictError
		end
	end

	## =============================InsteonCommand==============================
	## Description: Private class that instantates an Insteon Command Object
	## 				 TODO explain the ictl - Insteon Command Table List  
	##              class-wide instance data structure
	## =========================================================================	
	class InsteonCommand		
		@@ictl = InsteonCommandTableEntryList.new
		attr_accessor :ictCmdType, :strDeviceID, :bCmd2, :extData, :timestamp
		@ictCmdType
		@strDeviceID
		@bCmd2
		@extData
		@timestamp

		## TODO
		def initialize()
			@bCmd2 = 0
			@extData = nil
			touch()
		end

		## TODO
		def setCmdType(strCmdType)
			@ictCmdType = @@ictl.FindCmdType(strCmdType)
		end

		## TODO
		def setCmdTypeFromCmd1(bCmd1)
			@ictCmdType = @@ictl.FindCmdTypeFromCmd1(bCmd1)
		end

		## TODO
		def setCmdTypeFromIMCmd(bIMCmd)
			@ictCmdType = @@ictl.FindCmdTypeFromIMCmd(bIMCmd)
		end

		## TODO
		def touch()
			@timestamp = Time.now.to_f
		end

		## TODO
	   def touchFuture(delay)  # in seconds
	   	@timestamp = Time.now.to_f + delay
	   end

	   ## TODO
	   def ==(ic)
	   	if !ic.nil? && @ictCmdType == ic.ictCmdType
	   		if @bCmd2 == ic.bCmd2
	   			return @strDeviceID == ic.strDeviceID
	   		end
	   	end
	   	return false
	   end

	   ## TODO
	   def setCmd2(nArg)
	   	case ictCmdType.strCmdType.upcase
		   	when "ON", "FASTON"
		   		@bCmd2 = 0xFF
		   	when "SET"
		   		@bCmd2 = (nArg * 255) / 100
		   	when "EZIO_ON", "EZIO_OFF", "SPRINKLERON", "SPRINKLEROFF"
		   		@bCmd2 = nArg  # SubID
				else
					@bCmd2 = 0x00
			end
		end

		## TODO		
		def getLevel()
			return (@bCmd2 * 100) / 255
		end

		## TODO
		def getSubID()
			return @bCmd2
		end

		## TODO	
		def getInsteonMsg()
			rgbMsg = Array.new
			rgbMsg.push(0x02)
			rgbMsg.push(@ictCmdType.bIMCmd)
			case @ictCmdType.bIMCmd
			   when 0x62    # Send INSTEON Message
			   	rgbMsg.push(@strDeviceID[0..1].hex)
			   	rgbMsg.push(@strDeviceID[3..4].hex)
			   	rgbMsg.push(@strDeviceID[6..7].hex)
			   	rgbMsg.push(@ictCmdType.bCmd1 == 0x2F ? 0x1F : 0x0F)
			   	rgbMsg.push(@ictCmdType.bCmd1)
			   	rgbMsg.push(@bCmd2)
			   	if @ictCmdType.bCmd1 == 0x2F
			   		rgbMsg.concat(@extData)
			   	end
			   	### TODO dumpMessage("InsteonCommand::getInsteonMsg",rgbMsg)
			   when 0x64	#Start ALL-Linking
			   	rgbMsg.push(@ictCmdType.bCmd1)
			   	rgbMsg.push(0)
			   when 0x6F	# Manage ALL-Link Record
			   	rgbMsg.push(@ictCmdType.bCmd1)
			   	rgbMsg.push(@bCmd2)
				   rgbMsg.push(1)	# everything is in group 1
				   rgbMsg.push(@strDeviceID[0..1].hex)
				   rgbMsg.push(@strDeviceID[3..4].hex)
				   rgbMsg.push(@strDeviceID[6..7].hex)
				   rgbMsg.push(0)
				   rgbMsg.push(0)
				   rgbMsg.push(0)
				   rgbMsg.push(0)
			end
			return rgbMsg
		end
   end
   ## =============================InsteonCommand==============================

   ## ---------------------------Main Class Insteon----------------------------
   TX_ACK_DELAY = 0.3
   B_ACK = 0x06
   B_NACK = 0x15
   @exiting
   @ididIM
   @icToBeAcked
   @insteonProcess
   @port
   @postEventProcess
   @postEventQueue
   @receivedEventProcess
   @receivedCommandQueue
   @cmdQueue
   @link
   @plcInfo

   ## TODO comment?
   def initialize()
   	@exiting = false
   	@icToBeAcked = nil
   	@link = nil
   	@cmdQueue = Queue.new
   	@postEventQueue = Queue.new
   	@receivedCommandQueue = Queue.new
   	@plcInfo = nil

   	## Open Serial Port to talk to PowerLinc 2413u
		port_str = "/dev/ttyUSB0"  
		baud_rate = 19200 
		data_bits = 8
		stop_bits = 1
		parity = SerialPort::NONE
		@port = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)
		@port.flow_control = SerialPort::NONE
		@port.read_timeout = 100
		@port.binmode
		
	end

	## -------------------------Insteon::dumpMessage----------------------------
	## Debug output for neatly displaying Insteon raw bytes
	## TODO add option arguments to the CLI to turn on/off
	def dumpMessage(strText,msg)
		puts "Insteon Message =>[#{strText}](len=#{msg.length}):"
		printf ("[ ") 
		msg.each { |b| printf("%.2X ",b) }
		printf (" ]\n")
	end
	## -------------------------------------------------------------------------


	## -------------------------Insteon::createEventStream----------------------------
	def createEventStream()
		printf("\n\n---> enter createEventStream\n")
		begin
			Thread.new do
				puts 'Waiting for Sinatra to start before creating EventSource to receive events...'
				sleep 7  # TODO instead of sleep sense somehow that Sinatra has started
				puts 'Creating EventSource to receive events'
			  source = EventMachine::EventSource.new(@plcInfo['eventStreamUrl'].to_s)
			  source.message do |message|
			  	receivedCommand(message)
#			    puts "new message #{message}"
			  end
			  source.start # Start listening
			end
		rescue
			puts 'Error creating event stream'
		end

		printf("<--- exit createEventStream\n\n")
	end
	## -------------------------------------------------------------------------


	## -------------------------Insteon::savePLCInfo----------------------------
	def savePLCInfo(strPlcInfo)
		printf("\n\n---> enter savePLCInfo\n")
		puts strPlcInfo

		plcInfo = JSON.parse(strPlcInfo)

		if plcInfo.keys.include?("deviceId")
			@plcInfo = plcInfo
			File.write('plcInfo', strPlcInfo)
		else
			File.write('plcError', strPlcInfo)
		end
		printf("<--- exit savePLCInfo\n\n")
	end
	## -------------------------------------------------------------------------


	## -------------------------Insteon::getPLCInfo----------------------------
	def getPLCInfo()
		printf("\n\n---> enter getPLCInfo\n")
		return @plcInfo.to_json
	end
	## -------------------------------------------------------------------------


	## -------------------------Insteon::handlePLCInfo----------------------------
	def handlePLCInfo(insteonId)
		printf("\n\n---> enter handlePLCInfo\n")
		return if !@plcInfo.nil?

		begin
			if File.exist?('plcInfo')
				@plcInfo = JSON.parse(File.read('plcInfo'))
				@plcInfo["insteonId"] = insteonId.to_s
				@plcInfo["directUrl"] = 'http://' + localIp + ':8001'
				response = RestClient.patch TA_API_BASE + 'devices/' + @plcInfo["deviceId"].to_s , @plcInfo.to_json, :content_type => :json, :accept => :json
			else
				@plcInfo = JSON.parse('{"name":"Insteon Hub", "insteonId":"' + insteonId.to_s + '", "deviceTypeUuid":"f4cbaea5-e5de-429b-b46d-63d6266e5023", "directUrl":"http://' + localIp + ':8001"}')
				response = RestClient.post TA_API_BASE + 'devices', @plcInfo.to_json, :content_type => :json, :accept => :json
			end
			savePLCInfo(response)
			createEventStream()
		rescue
			puts 'Error reading PLCInfo'
		end
		printf("<--- exit handlePLCInfo\n\n")
	end
	## -------------------------------------------------------------------------

	## -------------------------Insteon::associate----------------------------
	def associate(strHomeKey)
		puts 'Insteon::associate'
		begin
			puts strHomeKey
			homeKey = JSON.parse(strHomeKey)
			response = RestClient.post TA_API_BASE + 'devices/' + @plcInfo["deviceId"].to_s + '/associate?access_token=' + @plcInfo["deviceToken"] , homeKey.to_json, :content_type => :json, :accept => :json
			savePLCInfo(response)
		rescue
			puts 'Error associating with homeKey'
		end
	end
	## -------------------------------------------------------------------------

	## -------------------------Insteon::getDeviceID----------------------------
	def getDeviceID()
		begin
			strDeviceID = ''
			strDeviceID += @port.getbyte.to_s(16).upcase.rjust(2,'0')
			strDeviceID += '.'
			strDeviceID += @port.getbyte.to_s(16).upcase.rjust(2,'0')
			strDeviceID += '.'
			strDeviceID += @port.getbyte.to_s(16).upcase.rjust(2,'0')
			return strDeviceID
		rescue
			return 'invalid'
		end
	end
	## -------------------------------------------------------------------------

	## -------------------------Insteon::
	def writeInsteonCommand(ic)
		printf("\n\n---> enter writeInsteonCommand\n")
		cRetries = 3
		rgbMsg = ic.getInsteonMsg()
		dumpMessage("writeInsteon Method",rgbMsg)
		begin 
			rgbMsg.each { |b| 
				@port.putc(b) 				
			}		
			puts "Send Insteon Cmd, Retry:#{cRetries}" 	
		end until :Nack != parseInsteonMsg() || --cRetries == 0
		printf("<--- exit writeInsteonCommand\n\n")
	end
	## -------------------------------------------------------------------------


	## -------------------------Insteon::
	def processInsteon()
		puts 'entering processInsteon'
		while !@exiting	
			if @cmdQueue.length > 0	
				@icToBeAcked = @cmdQueue.pop
				@icToBeAcked.touchFuture(TX_ACK_DELAY)
				writeInsteonCommand(@icToBeAcked)
			end			
			while :Ack == parseInsteonMsg() || (!@icToBeAcked.nil? && (@icToBeAcked.timestamp > Time.now.to_f))
			end			
		end
		puts 'exiting processInsteon'
	end
	## -------------------------------------------------------------------------


	## -------------------------Insteon::	
	def processPostEventQueue()
		puts 'entering processPostEventQueue'
		eventHash = @postEventQueue.pop
		while !@exiting
			puts eventHash
			begin
				response = RestClient.post TA_API_BASE + 'devices/' + @plcInfo["deviceId"].to_s + '/event?access_token=' + @plcInfo["deviceToken"], eventHash.to_json, :content_type => :json, :accept => :json
			rescue
			end
			eventHash = @postEventQueue.pop
		end
		puts 'exiting processPostEventQueue'
	end
	## -------------------------------------------------------------------------


	## -------------------------Insteon::		
	def postEvent(eventHash)
		@postEventQueue << eventHash
	end
	## -------------------------------------------------------------------------


	## -------------------------Insteon::processReceivedCommandQueue
	def processReceivedCommandQueue()
		puts 'entering processReceivedCommandQueue'
		strCommand = @receivedCommandQueue.pop
		while !@exiting
			processEventToPLC(strCommand)
			strCommand = @receivedCommandQueue.pop
		end
		puts 'exiting processReceivedCommandQueue'
	end
	## -------------------------------------------------------------------------


	## -------------------------Insteon::		
	def receivedCommand(strCommand)
		@receivedCommandQueue << strCommand
	end
	## -------------------------------------------------------------------------


	## -------------------------Insteon::	
	def sendInsteonCommand(strCmd)
		ic = InsteonCommand.new
		ic.setCmdType(strCmd)
		@cmdQueue << ic
	end
	## -------------------------------------------------------------------------

	## -------------------------Insteon::
	def sendInsteonCommandToDest(strCmd, strToID, nCmd2)
		ic = InsteonCommand.new
		ic.setCmdType(strCmd)
		ic.strDeviceID = strToID
		ic.setCmd2(nCmd2)
		@cmdQueue << ic
	end
	## -------------------------------------------------------------------------

	## -------------------------Insteon:: 
	def processInsteonAck(ic)
		case ic.ictCmdType.strCmdType.upcase
		when "SET"
			strJson = '<status>' + getDeviceJSON(ic.strDeviceID) + '<state Load="' + ic.getLevel().to_s + '"/></status>'
		when "ON"
			strJson = '<status>' + getDeviceJSON(ic.strDeviceID) + '<state Load="On"/></status>'
		when "OFF"
			strJson = '<status>' + getDeviceJSON(ic.strDeviceID) + '<state Load="Off"/></status>'
		end
		postEvent(strJson)
	end
	## -------------------------------------------------------------------------

	## -------------------------Insteon::
	def processInsteonCommand(ic)
		case ic.ictCmdType.strCmdType.upcase
		when "STATUS"
			strJson = '<event>' + getDeviceJSON(ic.strDeviceID) + '<action Load="' + ic.getLevel().to_s + '"/></event>'
		when "ON"
			strJson = '<event>' + getDeviceJSON(ic.strDeviceID) + '<action Load="On"/></event>'
		when "OFF"
			strJson = '<event>' + getDeviceJSON(ic.strDeviceID) + '<action Load="Off"/></event>'
		end
		postEvent(strJson)
	end
	## -------------------------------------------------------------------------

	## -------------------------Insteon:: 
	def toBeAckedType()
		if @icToBeAcked.nil?
			return "null"
		else
			return @icToBeAcked.ictCmdType.strCmdType
		end
	end
	## -------------------------------------------------------------------------

	## -------------------------Insteon:: 
	def handleInsteonCode(bAck)
		case bAck
		when B_ACK 
			return :Ack
		when B_NACK 
			return :Nack
		else
			return :Error
		end
	end

	## TODO explain?
	## -------------------------Insteon::parseInsteonMsg------------------------
	def parseInsteonMsg()
		bAck = B_NACK
		begin	
			bIMCmd = @port.getbyte			
			if 0x02 != bIMCmd			
				return :Nack
			end

			bIMCmd = @port.getbyte			
			ic = InsteonCommand.new			
			ic.setCmdTypeFromIMCmd(bIMCmd)
			case bIMCmd

			## -------INSTEON Standard Message Received-------
			when 0x50
				begin				
					msg = []			
					fromID = getDeviceID()				
					ic.strDeviceID = fromID
					fromID.split('.').each { |x| msg << x.to_i(16) }
					toID = getDeviceID()
					toID.split('.').each { |x| msg << x.to_i(16) }
					bFlags = @port.getbyte	
					msg << bFlags

					## TODO Explain?
					if "Status" == toBeAckedType() || "Set" == toBeAckedType()
						msg << @port.getbyte
						ic.bCmd2 = @port.getbyte
						msg << ic.bCmd2
					else
						cmdType = @port.getbyte
						msg << cmdType
						ic.setCmdTypeFromCmd1(@port.getbyte)
						ic.setCmd2(@port.getbyte)
						msg << ic.bCmd2
					end

					## TODO Explain?
					if (ic.ictCmdType.bCmd1 == 0x2F)
						puts 'what the?'
						14.times { |x| msg << @port.getbyte }
					end

					## TODO Explain?
					if 0xC0 == (bFlags & 0xE0)
						if ("PressHoldStop" == ic.ictCmdType.strCmdType)
							sendInsteonCommandToDest("Status", ic.strDeviceID, 0)
						elsif("PressHoldStart" != ic.ictCmdType.strCmdType)
							processInsteonCommand(ic)
						end
					elsif 0x20 == (bFlags & 0xE0)
						if ("Status" == toBeAckedType())
							@icToBeAcked.bCmd2 = ic.bCmd2
							processInsteonCommand(@icToBeAcked)
							@icToBeAcked = nil
						elsif ("Set" == toBeAckedType() || ic == @icToBeAcked)
							processInsteonAck(@icToBeAcked)
							@icToBeAcked = nil
						end
					end
					dumpMessage("MSG 0x50",msg)	
				rescue Exception => e
					puts e,"rescue issue with 0x50"
				end	
			## -----------------MSG 0x50----------------------
			## ---------INSTEON Extended Message Received-----
			when 0x51
				begin	
					puts 'test INSTEON Extended Message Received'
					msg = []
					25.times { |x| msg << @port.getbyte } 
					dumpMessage("MSG 0x51",msg)
				rescue Exception => e
					puts e,"rescue issue with 0x51"
				end
			## -----------------------------------------------
			## -----------ALL-Linking Completed---------------
			when 0x53
				begin
					msg = []
				   bFlags = @port.getbyte		# Link code
				   bGroup = @port.getbyte
				   msg << bFlags
				   msg << bGroup
				   toID = getDeviceID()
				   toID.split('.').each { |x| msg << x.to_i(16) }
					## dummy reads
					msg << @port.getbyte
					msg << @port.getbyte
					msg << @port.getbyte
					## TODO explain ?
					ic = InsteonCommand.new
					ic.setCmdType("ReadWrite-ALDB")
					ic.strDeviceID = toID
					ic.extData = Array.new
					14.times do |x|
						ic.extData.push(0)
					end
					@cmdQueue << ic

					if !@link.nil?
						linkHash = @link['link']
						if !linkHash.nil?
							linkHash['insteonId'] = toID							
							linkHash['protocol'] = 'insteon'
							response = RestClient.put TA_API_BASE + 'devices?access_token=' + @plcInfo["deviceToken"], linkHash.to_json, :content_type => :json, :accept => :json
							puts response
						end
					end
					dumpMessage("ALL-Linking Completed:0x53",msg)			
				rescue Exception => e
					puts e,"rescue issue with 0x53"	
				end		
			## -----------------------------------------------
			## -----------ALL-Link Record Response------------
		   when 0x57	# 			
		   	begin
		   		msg = []
				   bFlags = @port.getbyte		# flags
				   bGroup = @port.getbyte
				   toID = getDeviceID()
				   msg << bFlags
				   msg << bGroup
				   msg << toID.split('.').each { |x| msg << x.to_i(16) }	
				   msg << @port.getbyte
				   msg << @port.getbyte
				   msg << @port.getbyte
				   dumpMessage("MSG 0x57",msg) 
				rescue Exception => e
					puts e,"rescue issue with 0x57"	
				end		
			## -----------------------------------------------
			## --------ALL-Link Cleanup Status Report---------
		   when 0x58		
		   	begin
		   		msg = []
		   		bAck = @port.getbyte
		   		msg << bAck
		   		@icToBeAcked = nil
		   		dumpMessage("MSG 0x58",msg)
		   	rescue Exception => e
		   		puts e,"rescue issue with 0x58"	
		   	end					
			## -----------------------------------------------	
			## -------------------GetPLCInfo------------------	
		   when 0x60
		   	begin
		   		msg = []				
		   		@ididIM = getDeviceID()
		   		@ididIM.split('.').each { |x| msg << x.to_i(16) }
					msg << @port.getbyte # Device Category
					msg << @port.getbyte # Device Subcategory
					msg << @port.getbyte # Firmware Version
					bAck = @port.getbyte
					msg << bAck 
					if (B_ACK == bAck)
						@icToBeAcked = nil
					end

					handlePLCInfo(@ididIM.to_s)
				rescue Exception => e
					puts e,"rescue issue with 0x60"	
				end	
	
			## -----------------------------------------------
			## ------------Send ALL-Link Command---------------
			when 0x61
				begin
					msg = []	
				   msg << @port.getbyte	# group
				   msg << @port.getbyte	# command
				   msg << @port.getbyte	# command 2
				   bAck = @port.getbyte	# Ack 
				   msg << bAck
					## TODO explain?
					if (B_ACK == bAck)
						@icToBeAcked = nil
					end
				rescue Exception => e
					puts e,"rescue issue with 0x61"	
				end
			## -----------------------------------------------
			## ------Response from Send INSTEON Message-------
			when 0x62	
				begin
					msg = []
					ic.strDeviceID = getDeviceID()
					ic.strDeviceID.split('.').each { |x| msg << x.to_i(16) }
				   bFlags = @port.getbyte		# flags
				   msg << bFlags
				   cmd1 = @port.getbyte
				   ic.setCmdTypeFromCmd1(cmd1)
				   msg << cmd1
				   ic.bCmd2 = @port.getbyte
				   msg << ic.bCmd2
					## TODO explain ?
					if 0x10 == (bFlags & 0x10)
						14.times { |x| msg << @port.getbyte }
					end
					bAck = @port.getbyte
					msg << bAck
					dumpMessage("Response 0x62",msg)				            		
				rescue Exception => e
					puts e
					puts "rescue issue with 0x50"	
				end	
			# do not remove from retransmit queue, let ack from switch do it
			#				if (B_ACK != bAck)
			#					handleInsteonCode(bAck)
			#				end
			## -----------------------------------------------
			## ---------------Start ALL-Linking---------------
		   when 0x64	# Start ALL-Linking	
		   	begin	
		   		msg = []	
				   msg << @port.getbyte	# code
				   msg << @port.getbyte	# group
				   bAck = @port.getbyte
				   msg << bAck
				   if (B_ACK == bAck)
				   	@icToBeAcked = nil
				   end
		   		dumpMessage("Response 0x64",msg)				            		
				rescue Exception => e
					puts e
					puts "rescue issue with 0x64"	
				end		
			## -----------------------------------------------				
			## ----------------Reset the IM-------------------					
			when 0x67
				begin			
					msg = []
					bAck = @port.getbyte
					msg << bAck
					## TODO explain?
					if (B_ACK == bAck)
						@icToBeAcked = nil
					end
				dumpMessage("Response 0x67",msg)
			rescue Exception => e
				puts e
				puts "rescue issue with 0x67"	
			end		
			## -----------------------------------------------					
			## -----------Get First ALL-Link Record-----------	
			when 0x69	
				begin	
					msg = []
					bAck = @port.getbyte
					msg << bAck 
					@icToBeAcked = nil
				## TODO explain?
				if (B_ACK == bAck)
					# make call to get next record
					sendInsteonCommand("GetNextALL-LinkRecord")
				end
				dumpMessage("Response 0x69",msg)
			rescue Exception => e
				puts e
				puts "rescue issue with 0x69"	
			end
			## -----------------------------------------------
			## -----------------------------------------------	
		   when 0x6A	# Get Next ALL-Link Record	
		   	begin		
		   		msg = []
		   		bAck = @port.getbyte
		   		msg << bAck
		   		@icToBeAcked = nil
					## TODO explain?
					if (B_ACK == bAck)
						# make call to get next record
						sendInsteonCommand("GetNextALL-LinkRecord")
					end
					dumpMessage("Response 0x6A",msg)
				rescue Exception => e
					puts e, "rescue issue with 0x6A"	
				end
			## -----------------------------------------------
			## -----------Manage ALL-Link Record--------------	
			when 0x6F
				begin
					msg = []
					bControlCode = @port.getbyte
					msg << bControlCode
					if (0x40 == bControlCode)
						ic.setCmdType("ALL-LinkController")
					elsif (0x41 == bControlCode)
						ic.setCmdType("ALL-LinkResponder")
					else
						ic.setCmdType("Error")
					end
					ic.bCmd2 = @port.getbyte
					msg << ic.bCmd2
				   msg << @port.getbyte	# ALL-Link group
				   ic.strDeviceID = getDeviceID()
				   ic.strDeviceID.split('.').each { |x| msg << x.to_i(16) }
				   msg << @port.getbyte	# Link data
				   msg << @port.getbyte	# Link data
				   msg << @port.getbyte	# Link data
				   bAck = @port.getbyte
				   msg << bAck
				   if (B_ACK == bAck)
				   	@icToBeAcked = nil
				   end
				   dumpMessage("Response 0x6F",msg)
				rescue Exception => e
					puts e, "rescue issue with 0x6F"	
				end
			## -----------------------------------------------		

			else
				bAck = 0
			end
			## -----------------------------------------------
			rescue
			# do nothing
			end
		return handleInsteonCode(bAck)
   end
	## -------------------------------------------------------------------------

   def processEventToPLC(strCommand)
   	puts strCommand
   	begin
	   	command = JSON.parse(strCommand)
=begin
   		if (nodeRoot.name == 'test')
   			puts 'test here'
   			ic = InsteonCommand.new
   			ic.setCmdType("ReadWrite-ALDB")
   			ic.strDeviceID = "04.9B.C2"
   			ic.extData = Array.new
   			ic.extData.push(0)
   			ic.extData.push(0)
   			ic.extData.push(0x00)
   			ic.extData.push(0x00)
   			10.times do |x|
   				ic.extData.push(0)
   			end
   			@cmdQueue << ic
   			return
   		end
=end
   		if (command.has_key?("link"))
   			puts 'here - link'
   			@link = command
#				sendInsteonCommand("ALL-LinkResponder")
				sendInsteonCommand("ALL-LinkController")
			elsif (command.has_key?("device") && command['device'].has_key?("insteonId") && command.has_key?("action"))
				device = command['device']
				action = command['action']
				nArg = 0
				nArg = device['subid'] if (device.has_key?("subid"))

				begin
					strCommand = action['load']
					if ("on" != strCommand && "off" != strCommand)
						nArg = strCommand.to_i
						strCommand = "set"
					end
				rescue
				# do nothing
				end

				begin
					case device['protocol'].downcase
					when 'x10'
					when 'insteon'
						begin
							toID = device['insteonId']
							sendInsteonCommandToDest(strCommand, toID, nArg)
						end
					end
				rescue
					toID = command['insteonId']
					sendInsteonCommandToDest(strCommand, toID, nArg)
				end
			end
		rescue
		end
	end

	## TODO 
	def run()		
		@insteonProcess = Thread.new do
			processInsteon()
		end
		@postEventProcess = Thread.new do
			processPostEventQueue()
		end
		@receivedEventProcess = Thread.new do
			processReceivedCommandQueue()
		end

		## Send a quick test of on/off
		sendInsteonCommand("GetPLCInfo")
=begin
      sleep 3

		puts "Get My Insteon Engine Version"
		sendInsteonCommandToDest("GetInstEngVer","04.9B.C2", nil)
		sleep 2

		puts "Ping My Lamp!"
		sendInsteonCommandToDest("Ping","04.9B.C2", nil)
		sleep 2

		2.times do
			printf("\n")
			puts "Turn my Lamp ON!"
			sendInsteonCommandToDest("FastOn","04.9B.C2", nil)
			sleep 1
			puts "Turn my Lamp OFF!"
			sendInsteonCommandToDest("FastOff","04.9B.C2", nil)
			sleep 1
		end


		puts "Ramp my lamp up in 10 steps!"
		level = 0
		10.times do
			puts "Set Lamp to: #{level}"
			sendInsteonCommandToDest("Set","04.9B.C2", level)
			level = level + 10
			level > 255 ? 255 : level
			sleep 0.5
		end

		puts "Ramp my lamp down in 10 steps!"
		level = 100
		10.times do
			puts "Set Lamp to: #{level}"
			sendInsteonCommandToDest("Set","04.9B.C2", level)
			level = level - 10
			level < 0 ? 0 : level
			sleep 0.5
		end

		puts "Sleep for 3 seconds and then join all Threads"

		sleep 1
		## TODO need a better handshake to ensure all messages are flushed
		##  Could be tricky if the PLM gets an unsolicited message
		6.times do
			print(".")
			sleep 0.5
		end 
=end
		@postEventProcess.join
		@receivedEventProcess.join
		@insteonProcess.join

		puts 'InsteonPLM exiting'
		sleep 1
		@port.close
		sleep 1
	end

	def test()
		ic = InsteonCommand.new			
		ic.setCmdTypeFromIMCmd(0x69)
		puts ic.ictCmdType.strCmdType
		ic.setCmdTypeFromIMCmd(0x60)
		puts ic.ictCmdType.strCmdType
	end
end

inst = Insteon.new

Thread.new do
	inst.run
end

get '/' do 
	inst.getPLCInfo()
end

post '/' do 
	inst.receivedCommand(request.body.read)
end

post '/associate' do
	inst.associate(request.body.read)
end

