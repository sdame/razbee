# InsteonPLM.rb
# <registerProcess name="Insteon PLM" isGateway="true" version="0.6" workingDirectory="/think/ThinkDrivers" commandLine="ruby InsteonPLM.rb" autoStart="false"/>
require 'rubygems'
require 'nokogiri'
require 'serialport'
require 'thread'
# require 'timeout'
require 'net/http'
require 'byebug'


class Insteon
   class InsteonCommandType
      attr_accessor :strCmdType 
      @strCmdType
      attr_accessor :bIMCmd
      @bIMCmd
      attr_accessor :bCmd1
      @bCmd1
      attr_accessor :fMatchCmd1Lookup
      @fMatchCmd1Lookup
      attr_accessor :fAcked
      @fAcked

      def initialize(strCmdType, bIMCmd, bCmd1, fMatchCmd1Lookup, fAcked)
         @strCmdType = strCmdType
         @bIMCmd = bIMCmd
         @bCmd1 = bCmd1
         @fMatchCmd1Lookup = fMatchCmd1Lookup
         @fAcked = fAcked
      end

      def ==(ict)
         return ict.nil? ? false : (@bIMCmd == ict.bIMCmd) && (@bCmd1 == ict.bCmd1)
      end
   end

   class InsteonCommandTypeList < Array
      @ictError
      
      def initialize()
#           byebug   
         @ictError = InsteonCommandType.new("Error", 0, 0, false, false)
         push(InsteonCommandType.new("PressHoldStart", 0x50, 0x17, true, false))
         push(InsteonCommandType.new("PressHoldStop", 0x50, 0x18, true, false))
         push(InsteonCommandType.new("On", 0x62, 0x11, true, true))
         push(InsteonCommandType.new("Set", 0x62, 0x11, false, true))
         push(InsteonCommandType.new("Off", 0x62, 0x13, true, true))
         push(InsteonCommandType.new("Dim", 0x62, 0x16, true, true))
         push(InsteonCommandType.new("Bright", 0x62, 0x15, true, true))
         push(InsteonCommandType.new("FastOn", 0x62, 0x12, true, true))
         push(InsteonCommandType.new("FastOff", 0x62, 0x14, true, true))
         push(InsteonCommandType.new("SprinklerOn", 0x62, 0x40, true, true))
         push(InsteonCommandType.new("SprinklerOff", 0x62, 0x41, true, true))
         push(InsteonCommandType.new("EZIO_On", 0x62, 0x45, true, true))
         push(InsteonCommandType.new("EZIO_Off", 0x62, 0x46, true, true))
         push(InsteonCommandType.new("Status", 0x62, 0x19, true, true))

         push(InsteonCommandType.new("X10HouseUnit", 0x63, 0, false, false))
         push(InsteonCommandType.new("X10HouseCmd", 0x63, 0, false, true))

         push(InsteonCommandType.new("ResetPLM", 0x67, 0, false, false))
         push(InsteonCommandType.new("GetPLMInfo", 0x60, 0, false, true))
         push(InsteonCommandType.new("ReadWrite-ALDB", 0x62, 0x2F, true, true))
         push(InsteonCommandType.new("GetFirstALL-LinkRecord", 0x69, 0, false, true))
         push(InsteonCommandType.new("GetNextALL-LinkRecord", 0x6A, 0, false, true))
         push(InsteonCommandType.new("ALL-LinkController", 0x64, 0x01, false, true))
         push(InsteonCommandType.new("ALL-LinkResponder", 0x64, 0x00, false, true))
         push(InsteonCommandType.new("ALL-LinkStart", 0x64, 0x03, false, true))

         push(InsteonCommandType.new("EZIO-OutputOff", 0x62, 0x46, true, true))
         push(InsteonCommandType.new("EZIO-AlarmDataRequest", 0x62, 0x47, true, true))
         push(InsteonCommandType.new("EZIO-WriteOutputPort", 0x62, 0x48, true, true))
         push(InsteonCommandType.new("EZIO-ReadInputPort", 0x62, 0x49, true, true))
         push(InsteonCommandType.new("EZIO-GetSensorValue", 0x62, 0x4A, true, true))
         push(InsteonCommandType.new("EZIO-AlarmDataRequest", 0x62, 0x47, true, true))
         push(InsteonCommandType.new("EZIO-SetSensorAlarmOff->On", 0x62, 0x4B, true, true)) #alarm activated from Off to On event
         push(InsteonCommandType.new("EZIO-SetSensorAlarmOn->Off", 0x62, 0x4C, true, true)) #alarm activated with On to Off event
         push(InsteonCommandType.new("EZIO-WriteConfigPort", 0x62, 0x4D, true, true))
         push(InsteonCommandType.new("EZIO-ReadConfigPort", 0x62, 0x4E, true, true))
         push(InsteonCommandType.new("EZIO-Controls", 0x62, 0x4F, true, true)) #Various sub-commands available
      end

      def FindCmdType(strCmdType)
         ret = select {|ict| ict.strCmdType.upcase == strCmdType.upcase}
         return ret.any? ? ret[0] : @ictError
      end

      def FindCmdTypeFromCmd1(bCmd1)
         ret = select {|ict| ict.fMatchCmd1Lookup && ict.bCmd1 == bCmd1}
         return ret.any? ? ret[0] : @ictError
      end
      
      def FindCmdTypeFromIMCmd(bIMCmd)
         ret = select {|ict| ict.bIMCmd == bIMCmd}
         return ret.any? ? ret[0] : @ictError
      end
   end

   class InsteonCommand    
      @@ictl = InsteonCommandTypeList.new

      attr_accessor :ictCmdType 
      @ictCmdType
      attr_accessor :strDeviceID
      @strDeviceID
      attr_accessor :bCmd2
      @bCmd2
      attr_accessor :extData
      @extData
      #@x10Cmd
      attr_accessor :timestamp
      @timestamp

      def initialize()
         @bCmd2 = 0
         @extData = nil
         touch()
      end

      def setCmdType(strCmdType)
         @ictCmdType = @@ictl.FindCmdType(strCmdType)
      end
      
      def setCmdTypeFromCmd1(bCmd1)
         @ictCmdType = @@ictl.FindCmdTypeFromCmd1(bCmd1)
      end
      
      def setCmdTypeFromIMCmd(bIMCmd)
         @ictCmdType = @@ictl.FindCmdTypeFromIMCmd(bIMCmd)
      end
      
      def touch()
         @timestamp = Time.now.to_f
      end

      def touchFuture(delay)  # in seconds
         @timestamp = Time.now.to_f + delay
      end

      def ==(ic)
         if !ic.nil? && @ictCmdType == ic.ictCmdType
            if @bCmd2 == ic.bCmd2
               return @strDeviceID == ic.strDeviceID
            end
         end
         
         return false
      end

      def setCmd2(nArg)
         case ictCmdType.strCmdType.upcase
         when "ON", "FASTON"
            @bCmd2 = 0xFF
         when "SET"
            @bCmd2 = (nArg * 255) / 100
         when "EZIO_ON", "EZIO_OFF", "SPRINKLERON", "SPRINKLEROFF"
            @bCmd2 = nArg  # SubID
         else
            @bCmd2 = 0x00
         end
      end

      def getLevel()
         return (@bCmd2 * 100) / 255
      end

      def getSubID()
         return @bCmd2
      end
      
      def getInsteonMsg()
         rgbMsg = Array.new
         rgbMsg.push(0x02)
         rgbMsg.push(@ictCmdType.bIMCmd)
         case @ictCmdType.bIMCmd
         when 0x62    # Send INSTEON Message
            rgbMsg.push(@strDeviceID[0..1].hex)
            rgbMsg.push(@strDeviceID[3..4].hex)
            rgbMsg.push(@strDeviceID[6..7].hex)
            rgbMsg.push(@ictCmdType.bCmd1 == 0x2F ? 0x1F : 0x0F)
            rgbMsg.push(@ictCmdType.bCmd1)
            rgbMsg.push(@bCmd2)
            if @ictCmdType.bCmd1 == 0x2F
               rgbMsg.concat(@extData)
            end
            
            puts rgbMsg
         when 0x64   #Start ALL-Linking
            rgbMsg.push(@ictCmdType.bCmd1)
            rgbMsg.push(0)
         when 0x6F   # Manage ALL-Link Record
            rgbMsg.push(@ictCmdType.bCmd1)
            rgbMsg.push(@bCmd2)
            rgbMsg.push(1) # everything is in group 1
            rgbMsg.push(@strDeviceID[0..1].hex)
            rgbMsg.push(@strDeviceID[3..4].hex)
            rgbMsg.push(@strDeviceID[6..7].hex)
            rgbMsg.push(0)
            rgbMsg.push(0)
            rgbMsg.push(0)
            rgbMsg.push(0)
         end
         
         return rgbMsg
      end
   end   

   TX_ACK_DELAY = 0.3
   B_ACK = 0x06
   B_NACK = 0x15
   
   @exiting
   @ididIM
   @icToBeAcked
   @insteonProcess
   @pipeProcess
   @port
   @postXMLProcess
   @postXMLQueue
   @cmdQueue
   @linkXML

   def initialize()
#        byebug
      @exiting = false
      @icToBeAcked = nil
      @linkXML = nil

      @cmdQueue = Queue.new
      @postXMLQueue = Queue.new
      
#     @port = SerialPort.new("/dev/ttyUSB0", 19200)
#byebug  
      port_str = "/dev/ttyUSB0"
      baud_rate = 19200 
      data_bits = 8
      stop_bits = 1
      parity = SerialPort::NONE
      @port = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)
      @port.flow_control = SerialPort::NONE
      @port.read_timeout = 100
      @port.binmode

      ## Local file system FIFO for an Insteon Queue
      if !File.exist?('InsteonPLMPipe')
         system('mkfifo -m 0660 InsteonPLMPipe')
      end
   end

   def getDeviceID()
      begin
         strDeviceID = @port.getbyte.to_s(16).upcase.rjust(2,'0')
         strDeviceID += '.'
         strDeviceID += @port.getbyte.to_s(16).upcase.rjust(2,'0')
         strDeviceID += '.'
         strDeviceID += @port.getbyte.to_s(16).upcase.rjust(2,'0')
         return strDeviceID
      rescue
         return 'invalid'
      end
   end

   def getDeviceXML(strDeviceID)
      return '<device gateway="Insteon PLM" protocol="Insteon" id="' + strDeviceID + '"/>'
   end

   def writeInsteonCommand(ic)
      puts 'enter writeInsteonCommand'
      cRetries = 3
      rgbMsg = ic.getInsteonMsg()
      puts rgbMsg
      begin 
         rgbMsg.each { |b| 
            @port.putc(b)           
         } 
         sleep(0.5)     
         puts "Send with Retry:#{cRetries}" 
      end until :Nack != parseInsteonMsg() || --cRetries == 0
      puts 'exit writeInsteonCommand'
   end

   def processInsteon()
      puts 'entering processInsteon'
      while !@exiting   
         if @cmdQueue.length > 0 
            @icToBeAcked = @cmdQueue.pop
            @icToBeAcked.touchFuture(TX_ACK_DELAY)
            writeInsteonCommand(@icToBeAcked)
         end         
         while :Ack == parseInsteonMsg() || (!@icToBeAcked.nil? && (@icToBeAcked.timestamp > Time.now.to_f))
         end         
      end
      puts 'exiting processInsteon'
   end
   
   def processPipe()
      puts 'entering processPipe'
      input = open("InsteonPLMPipe", "r+")
      while !@exiting
         data = input.readline "\0"    
         processXML(data)
      end
      puts 'exiting processPipe'
   end

   def processPostXMLQueue()
      puts 'entering processPostXMLQueue'
#     uri = URI('http://localhost:9205')
      strXml = @postXMLQueue.pop
      while !@exiting
         puts strXml
#        Net::HTTP.post_form(uri, 'xml' => strXml)
         strXml = @postXMLQueue.pop
      end
      puts 'exiting processPostXMLQueue'
   end
   
   def postXML(strXML)
      @postXMLQueue << strXML
   end
   
   def sendInsteonCommand(strCmd)
      ic = InsteonCommand.new
      ic.setCmdType(strCmd)
      @cmdQueue << ic
   end

   def sendInsteonCommandToDest(strCmd, strToID, nCmd2)
      ic = InsteonCommand.new
      ic.setCmdType(strCmd)
      ic.strDeviceID = strToID
      ic.setCmd2(nCmd2)
      @cmdQueue << ic
   end

   def processInsteonAck(ic)
      case ic.ictCmdType.strCmdType.upcase
      when "SET"
         strXML = '<status>' + getDeviceXML(ic.strDeviceID) + '<state Load="' + ic.getLevel().to_s + '"/></status>'
      when "ON"
         strXML = '<status>' + getDeviceXML(ic.strDeviceID) + '<state Load="On"/></status>'
      when "OFF"
         strXML = '<status>' + getDeviceXML(ic.strDeviceID) + '<state Load="Off"/></status>'
      end
      postXML(strXML)
   end
   
   def processInsteonCommand(ic)
      case ic.ictCmdType.strCmdType.upcase
      when "STATUS"
         strXML = '<event>' + getDeviceXML(ic.strDeviceID) + '<action Load="' + ic.getLevel().to_s + '"/></event>'
      when "ON"
         strXML = '<event>' + getDeviceXML(ic.strDeviceID) + '<action Load="On"/></event>'
      when "OFF"
         strXML = '<event>' + getDeviceXML(ic.strDeviceID) + '<action Load="Off"/></event>'
      end
      postXML(strXML)
   end
   
   def toBeAckedType()
      if @icToBeAcked.nil?
         return "null"
      else
         return @icToBeAcked.ictCmdType.strCmdType
      end
   end
   
   def handleInsteonCode(bAck)
      case bAck
      when B_ACK 
         return :Ack
      when B_NACK 
         return :Nack
      else
         return :Error
      end
   end

   def parseInsteonMsg()
      bAck = B_NACK
      begin 
         bIMCmd = @port.getbyte        
         if 0x02 != bIMCmd       
            return :Nack
         end

         bIMCmd = @port.getbyte        
         ic = InsteonCommand.new       
         ic.setCmdTypeFromIMCmd(bIMCmd)

         case bIMCmd
         when 0x50  # INSTEON Standard Message Received  
            puts 'test here receive standard'
            ic.strDeviceID = getDeviceID()
            toID = getDeviceID()
            bFlags = @port.getbyte  
            puts bFlags
            if "Status" == toBeAckedType() || "Set" == toBeAckedType()
               @port.getbyte
               ic.bCmd2 = @port.getbyte
            else
               ic.setCmdTypeFromCmd1(@port.getbyte)
               ic.setCmd2(@port.getbyte)
            end
            puts ic.strDeviceID
            puts ic.ictCmdType.strCmdType
            if (ic.ictCmdType.bCmd1 == 0x2F)
               puts 'what the?'
               14.times do |x|
                  putc @port.getbyte.to_s(16).upcase.rjust(2,'0')
                  putc ' '
               end
               puts ' '
            end

            if 0xC0 == (bFlags & 0xE0)
               if ("PressHoldStop" == ic.ictCmdType.strCmdType)
                  sendInsteonCommandToDest("Status", ic.strDeviceID, 0)
               elsif("PressHoldStart" != ic.ictCmdType.strCmdType)
                  processInsteonCommand(ic)
               end
            elsif 0x20 == (bFlags & 0xE0)
               if ("Status" == toBeAckedType())
                  @icToBeAcked.bCmd2 = ic.bCmd2
                  processInsteonCommand(@icToBeAcked)
                  @icToBeAcked = nil
               elsif ("Set" == toBeAckedType() || ic == @icToBeAcked)
                  processInsteonAck(@icToBeAcked)
                  @icToBeAcked = nil
               end
            end

         when 0x51  # INSTEON Extended Message Received  
            puts 'test here receive'
            25.times do |x|
               putc @port.getbyte.to_s(16).upcase.rjust(2,'0')
               putc ' '
            end
            puts ' '

         when 0x53   # ALL-Linking Completed       
            puts 'ALL-Linking Completed   '
                bFlags = @port.getbyte    # Link code
                bGroup = @port.getbyte
                toID = getDeviceID()
                @port.getbyte
                @port.getbyte
                @port.getbyte

                if !@linkXML.nil?
                  strXML = '<config><device gateway="Insteon PLM" protocol="Insteon" id="' + toID + '" type="'
                  strXML += @linkXML['type'] + '"/><state linked="true" location="'
                  strXML += @linkXML['location'] + '"'
                  if !@linkXML['name'].nil?
                     strXML += ' name="' + @linkXML['name'] + '"'
                  end
                  strXML += '/></config>'
                  postXML(strXML)

                  ic = InsteonCommand.new
                  ic.setCmdType("ReadWrite-ALDB")
                  ic.strDeviceID = toID
                  ic.extData = Array.new
                  14.times do |x|
                     ic.extData.push(0)
                  end
                  @cmdQueue << ic
                end

         when 0x57   # ALL-Link Record Response       
                bFlags = @port.getbyte    # flags
                bGroup = @port.getbyte
                toID = getDeviceID()
                @port.getbyte
                @port.getbyte
                @port.getbyte

         when 0x58   # ALL-Link Cleanup Status Report       
            bAck = @port.getbyte
            @icToBeAcked = nil
            
         when 0x60   # GetPLMInfo         
            @ididIM = getDeviceID()
            @port.getbyte # Device Category
            @port.getbyte # Device Subcategory
            puts @port.getbyte # Firmware Version
            bAck = @port.getbyte

            if (B_ACK == bAck)
               @icToBeAcked = nil
            end   
            
            strXML = '<config>
            <device name="Insteon PLM" type="Insteon PLM" isGateway="true"/>
            <state backLink="pipe://InsteonPLMPipe" id="' + @ididIM + '"/>
            </config>'

#           strXML = '<config><device name="InsteonPLM" type="gateway" id="' + @ididIM + '" backLink="file://~/ThinkAutomatic/drivers/InsteonPLMPipe"/><state online="true"/></config>'
#           postXML(strXML)
#           strXML = '<config><type name="SwitchLinc V2" gateway="InsteonPLM"><action attribute="load" active="on" inactive="off" rangeLow="0" rangeHigh="100" learnByRecall="true" learnByGuess="true"/></type></config>'
postXML(strXML)

=begin
strXML = '<config>
<device name="Insteon PLM">
<type name="Insteon PLM" showLinkTo="false"/>
<type name="SwitchLinc V2">
<action name="Load" activate="On" deactivate="Off" rangeLow="0" rangeHigh="100" learn="true" trigger="false"/>
</type>
<type name="Lamp Module">
<action name="Load" activate="On" deactivate="Off" rangeLow="0" rangeHigh="100" learn="true" trigger="false"/>
</type>
</device>
</config>'
postXML(strXML)
=end
         when 0x61   # Send ALL-Link Command       
            @port.getbyte  # group
            @port.getbyte  # command
            @port.getbyte  # command 2
            bAck = @port.getbyte #

            if (B_ACK == bAck)
               @icToBeAcked = nil
            end

         when 0x62   # Send INSTEON Message  
            puts 'test shit'
            ic.strDeviceID = getDeviceID()
            puts ic.strDeviceID
                bFlags = @port.getbyte    # flags
                puts bFlags
                ic.setCmdTypeFromCmd1(@port.getbyte)
                puts ic.ictCmdType.strCmdType
                ic.bCmd2 = @port.getbyte
                puts ic.bCmd2
                if 0x10 == (bFlags & 0x10)
                  14.times do |x|
                     puts @port.getbyte
                  end
                end

                bAck = @port.getbyte
                puts bAck

            # do not remove from retransmit queue, let ack from switch do it
#           if (B_ACK != bAck)
#              handleInsteonCode(bAck)
#           end

         when 0x64   # Start ALL-Linking        
            @port.getbyte  # code
            @port.getbyte  # group
            bAck = @port.getbyte

            if (B_ACK == bAck)
               @icToBeAcked = nil
            end

         when 0x67   # Reset the IM       
            bAck = @port.getbyte
            if (B_ACK == bAck)
               @icToBeAcked = nil
            end

         when 0x69   # Get First ALL-Link Record         
            bAck = @port.getbyte
            @icToBeAcked = nil
            if (B_ACK == bAck)
               # make call to get next record
               sendInsteonCommand("GetNextALL-LinkRecord")
            end
            
         when 0x6A   # Get Next ALL-Link Record       
            bAck = @port.getbyte
            @icToBeAcked = nil
            if (B_ACK == bAck)
               # make call to get next record
               sendInsteonCommand("GetNextALL-LinkRecord")
            end
            
         when 0x6F   # Manage ALL-Link Record         
            bControlCode = @port.getbyte
            if (0x40 == bControlCode)
               ic.setCmdType("ALL-LinkController")
            elsif (0x41 == bControlCode)
               ic.setCmdType("ALL-LinkResponder")
            else
               ic.setCmdType("Error")
            end

            ic.bCmd2 = @port.getbyte
            @port.getbyte  # ALL-Link group
            ic.strDeviceID = getDeviceID()
            @port.getbyte  # Link data
            @port.getbyte  # Link data
            @port.getbyte  # Link data
            bAck = @port.getbyte

            if (B_ACK == bAck)
               @icToBeAcked = nil
            end
         else
            bAck = 0
         end
      rescue
# do nothing
end

return handleInsteonCode(bAck)
end

def processXML(strXml)
   begin
      nodeRoot = Nokogiri::XML(strXml).children[0]

      if (nodeRoot.name == 'exit')
         strXML = '<config><device gateway="Insteon PLM" id="' + @ididIM + '"/><state online="false"/></config>'
         postXML(strXML)
         @exiting = true
         return
      end

      if (nodeRoot.name == 'test')
         puts 'test here'
         ic = InsteonCommand.new
         ic.setCmdType("ReadWrite-ALDB")
         ic.strDeviceID = "04.9B.C2"
         ic.extData = Array.new
         ic.extData.push(0)
         ic.extData.push(0)
         ic.extData.push(0x00)
         ic.extData.push(0x00)
         10.times do |x|
            ic.extData.push(0)
         end
         @cmdQueue << ic
         return
      end

      if (nodeRoot.name == 'link')
         puts 'here - link'
         @linkXML = nodeRoot
#           sendInsteonCommand("ALL-LinkResponder")
sendInsteonCommand("ALL-LinkController")
else
   nodeDevice = nodeRoot.xpath('//device')[0]
   nodeAction = nodeRoot.xpath('//action')[0]
   begin 
      nArg = nodeDevice['subid']
   rescue
      nArg = 0
   end

   begin
      strCommand = nodeAction['Load']
      if ("On" != strCommand && "Off" != strCommand)
         nArg = strCommand.to_i
         strCommand = "set"
      end
   rescue
               # do nothing
            end

            begin
               case nodeDevice['protocol'].downcase
               when 'x10'
               when 'insteon'
                  begin
                     toID = nodeDevice['id']
                     sendInsteonCommandToDest(strCommand, toID, nArg)
                  end
               end
            rescue
               toID = nodeDevice['id']
               sendInsteonCommandToDest(strCommand, toID, nArg)
            end
         end
      rescue
      end
   end
   
   def run()   
#  byebug   
      @insteonProcess = Thread.new do
         processInsteon()
      end

      @pipeProcess = Thread.new do
         processPipe()
      end

      @postXMLProcess = Thread.new do
         processPostXMLQueue()
      end
      
      sendInsteonCommand("GetPLMInfo")

      3.times do
         sendInsteonCommandToDest("FastOn","04.9B.C2", nil)
         sleep 2
         sendInsteonCommandToDest("FastOff","04.9B.C2", nil)
      end

      @postXMLProcess.join
      @insteonProcess.join
      @pipeProcess.join
      
      puts 'InsteonPLM exiting'
      sleep 1
      @port.close
      sleep 1
   end
   
   def test()
      ic = InsteonCommand.new       
      ic.setCmdTypeFromIMCmd(0x69)
      puts ic.ictCmdType.strCmdType
      ic.setCmdTypeFromIMCmd(0x60)
      puts ic.ictCmdType.strCmdType
   end

end

inst = Insteon.new

#byebug
#inst.test
inst.run