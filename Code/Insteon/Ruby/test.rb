require 'byebug'

class Test
   attr_accessor :foo, :bar, :ti1

   def initialize(x,y)
      @foo = x
      @bar = y
      @ti1 = TestInner1.new
      @ti1.square
      puts @ti1.pi, @ti1.pi2
   end

   def p
      puts "arg1:#{@foo},arg2:#{@bar}"
   end

   class TestInner1
      attr_accessor :pi, :pi2
      def initialize
         @pi = 3.14159
      end

      def square
         @pi2 = @pi*@pi
      end
   end
end
T = Test.new(42, 69)
T.p
