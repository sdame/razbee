'use strict';

var http = require('http');
var os = require('os');
var ifaces = os.networkInterfaces();
var request = require('request');
var sp = require("serialport");
var SerialPort = sp.SerialPort
var EventSource = require('eventsource');
var fs = require('fs');

var serialPort = new SerialPort("/dev/ttyAMA0", {
  parser: sp.parsers.readline("\n"),
  baudrate: 38400
}, false); // this is the openImmediately flag [default is true] 

var urlToThinkAutomatic = 'https://thinkautomatic.io/api';
// /event-stream/UGsV8x179Ue74Nwjk5wRoij1wPVDruZMBSrJQsQZN5gRSdBDi9eXrOYon42fzb2B';

var ipAddress;
var homeKey = {};

Object.keys(ifaces).forEach(function (ifname) {
  var alias = 0;

  ifaces[ifname].forEach(function (iface) {
    if ('IPv4' !== iface.family || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }

    if (alias >= 1) {
      // this single interface has multiple ipv4 addresses
      console.log(ifname + ':' + alias, iface.address);
    } else {
      // this interface has only one ipv4 adress
      console.log(ifname, iface.address);
    }

    ipAddress = iface.address;
    ++alias;
  });
});


// Configure our HTTP server to respond with Hello World to all requests.
var server = http.createServer(function (request, response) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.end(JSON.stringify(homeKey));
});

// Listen on port 8000, IP defaults to 127.0.0.1
server.listen(8000);

var homeKeyUrl = "http://" + ipAddress + ":8000/";

// Put a friendly message on the terminal
console.log("Server running at " + homeKeyUrl);


function patchDevice(device, hubDeviceInfo)
{
  if (device['deviceId']) {
    request.patch({url: urlToThinkAutomatic + '/Devices/' + device['deviceId'], qs: { access_token: hubDeviceInfo['deviceToken'] }, form: device}, 
      function(err,httpResponse,body) { 
        console.log('patch ' + body);
//        deviceInfo = JSON.parse(body);  
      }
    );    
  }
}


function run(hubDeviceInfo)
{
  var urlToHubChangeStream = urlToThinkAutomatic + '/event-stream/' + hubDeviceInfo['deviceToken']

  serialPort.open(function (error) {
    if ( error ) {
      console.log('failed to open: '+error);
    } else {
      console.log('open');
      serialPort.on('data', function(data) {
        console.log('serial data received');
        console.log(data.toString());
        var dataJSON = JSON.parse(data.toString());
        console.log(dataJSON);
        if (dataJSON['device'])
        {
          var device = dataJSON['device'];
          if (!device || !device['deviceId'])
          {
            request.get({url: urlToThinkAutomatic + '/Devices/findOne', qs: { access_token: hubDeviceInfo['deviceToken'], filter: device}}, 
              function(err,httpResponse,body) { 
                var deviceInfo = JSON.parse(body);  
                console.log(deviceInfo);
                if (!deviceInfo || !deviceInfo['deviceId'])
                {
                  request.post({url: urlToThinkAutomatic + '/Devices', qs: { access_token: hubDeviceInfo['deviceToken'] }, form: device}, 
                    function(err,httpResponse,body) { 
                      console.log('post ' + body);
                      deviceInfo = JSON.parse(body);  
                    }
                  );
                }
                else
                {
                  patchDevice(device, hubDeviceInfo);
                }
              }
            );
          }
          else
          {
            patchDevice(device, hubDeviceInfo);            
          }
        }
      });

      console.log(urlToHubChangeStream);

/*      request
        .get(urlToHubChangeStream)
        .on('response', function(response) {
          console.log(response.statusCode) // 200
          console.log(response.headers['content-type']) // 'image/png'
        });

*/
      var srcHub = new EventSource(urlToHubChangeStream);
      srcHub.onmessage = function(e) {
        console.log(e.data);
//        var data = JSON.parse(e.data);
//        console.log(JSON.stringify(e.data)); // the change object
        serialPort.write(e.data + '\0', function(err, results) {
//          console.log('err ' + err);
//          console.log('results ' + results);
        });        
      };
      srcHub.onerror = function() {
        console.log('ERROR!');
      };
/*      srcHub.addEventListener('data', function(msg) {
        console.log(msg);
        var data = JSON.parse(msg.data);
        console.log(JSON.stringify(data)); // the change object
        serialPort.write(JSON.stringify(data) + '\0', function(err, results) {
//          console.log('err ' + err);
//          console.log('results ' + results);
        });        
      }); */
    }
  });
}


fs.readFile('/home/pi/ThinkHub/hubDeviceInfo', 'utf8', function (err,data) {
  var hubDeviceInfo;

  if (err) {
    console.log('creating new home');

    request.post({url: urlToThinkAutomatic + '/homes', form: {name:'Think Automatic Home', homeKeys:[{keyUrl:homeKeyUrl}]}}, function(err,httpResponse,body) { 
      if (!err)
      {
        var home = JSON.parse(body);
        console.log(body);

        console.log('creating new hub');
        homeKey = home['homeKeys'][0];

        request.post({url: urlToThinkAutomatic + '/Devices', qs: { access_token: homeKey.token }, form: 
          {name:'Think Automatic Hub', deviceTypeUuid: '0500e698-e602-485e-b680-b61241b89059',
           homeId: home['homeId']}}, function(err,httpResponse,body) { 
              hubDeviceInfo = JSON.parse(body);
              hubDeviceInfo['homeKey'] = homeKey;
              fs.writeFileSync('/home/pi/ThinkHub/hubDeviceInfo', JSON.stringify(hubDeviceInfo));
              run(hubDeviceInfo);
        });
      }
    });
  }
  else
  {
    hubDeviceInfo = JSON.parse(data);
    homeKey = hubDeviceInfo['homeKey'];
    homeKey['keyUrl'] = homeKeyUrl;
    request.patch({url: urlToThinkAutomatic + '/homes/homeKey', form: {token:homeKey['token'], keyUrl:homeKeyUrl}}, function(err,httpResponse,body) { 
      run(hubDeviceInfo);
    });
  }
});


