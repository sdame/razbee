import serial, time

inByte = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
startMsg = '\x02'

Table = {}
Table['\x50'] = 11   # 0x50 means its a standard insteon message, length 11 (9 bytes plus the 0x02 0x50)
Table['\x51'] = 25   # 0x51 means its an extended length insteon message, length 25  (23 bytes plus the 0x02 0x50)
Table['\x62'] = 9    # 0x62 means it's echoing back an insteon command that we sent, length 9 (or would be 23 if it was an extended message, we asume STANDARD ONLY for now)
Table['\x52'] = 4    # 0x52 means an X10 message was received
Table['\x53'] = 10   # 0x53 means ALL-linking Completed
Table['\x54'] = 3    # 0x54 means Button Event was reported
Table['\x55'] = 2    # 0x55 means a User Reset was detected
Table['\x56'] = 7    # 0x56 means an ALL-Link Cleanup Failure was reported
Table['\x57'] = 10   # 0x57 means an All-Link Record response
Table['\x58'] = 3    # 0x58 means an All-Link Cleanup status report

def ByteToHex(byteStr):
   return ''.join( [ "%02X " % ord( x ) for x in byteStr ] ).strip()

def listen_to_pylinc():
   count = 0
   msgLength = 30

   insteonSerial = serial.Serial(
         port = '/dev/ttyUSB0',
         baudrate = 19200,
         parity = serial.PARITY_NONE,
         stopbits = serial.STOPBITS_ONE,
         bytesize = serial.EIGHTBITS,
         timeout = 30
         )

   print "Starting..."
   time.sleep(1)
   print "Ready.\nWaiting for message..."

   while True:
      inByte[count] = insteonSerial.read()

      if inByte[count] == '\x02':
         count = 0

      if count == 1:
         if inByte[count] in Table:
            msgLength = Table[inByte[count]] - 1
            count = count + 1

      if count > msgLength:
         count = 0

      if inByte[1] == '\x50':
         state = ""
      if inByte[9] == '\x13' or inByte[9] == '\x14':
         state = "Off"
      elif inByte[9] == '\x11' or inByte[9] == '\x12':
         state = "On"

      if state != "":
         deviceid = "%s%s%s" % (ByteToHex(inByte[2]), ByteToHex(inByte[3]), ByteToHex(inByte[4]))
         print deviceid

      if __name__ == "__main__":
         listen_to_pylinc()
